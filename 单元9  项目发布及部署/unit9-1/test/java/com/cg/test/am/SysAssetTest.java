package com.cg.test.am;

import com.cg.test.am.service.SysAssetService;
import com.cg.test.am.vo.response.SysAssetResp;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import javax.annotation.Resource;

@SpringBootTest
public class SysAssetTest {
    @Resource
    private SysAssetService sysAssetService;

    @Test
    public void findAssetByIdTest(){
        SysAssetResp assetResp = sysAssetService.find((long) 1400);
        Assert.notNull(assetResp,"没有根据id找到资产");
    }
}
