package com.demo.library.service;


import com.demo.library.entity.Publisher;

import java.util.List;

public interface PublisherService {
    List<Publisher> findAll();
    Publisher getById(Integer id);
    void save(Publisher publisher);
    void update(Publisher publisher);
    void deleteById(Integer id);
    List<Publisher> findByPublisherNameContains(String publisherName);

}
