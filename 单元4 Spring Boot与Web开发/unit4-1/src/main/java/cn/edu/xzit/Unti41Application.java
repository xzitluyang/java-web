package cn.edu.xzit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Unti41Application {

    public static void main(String[] args) {
        SpringApplication.run(Unti41Application.class, args);
    }

}
