package cn.edu.xzit.controller;

import cn.edu.xzit.vo.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 *返回用户数据的JSON数据
 */
@Controller
//@RequestMapping("/user")
public class UserJSONController {
    @PostMapping( "/getUser")
    public User getUser(User user){
          return user;
    }


    @RequestMapping (path = "/userReg",method = RequestMethod.GET)
    public ModelAndView userReg(){
        return new ModelAndView("userReg");
    }


    @RequestMapping("/")
    public ModelAndView Hello(){
        ModelAndView mv=new ModelAndView("hello");
        mv.addObject("name","张三");
       return mv;
    }
}

