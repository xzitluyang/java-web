package cn.edu.xzit.controller;

import cn.edu.xzit.vo.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
public class TestController {
    @RequestMapping("/")
    public String test1(Map map, HttpServletRequest request) {

        map.put("message", "aaaaaaaa");
        map.put("num", 123);
        map.put("str1", "off");
        map.put("str2", "no");
        User user = new User();
        user.setId("1001");
        user.setUsername("zhang");
        user.setRole("admin");  //管理员
        User user2 = new User();
        user2.setId("1002");
        user2.setUsername("li");
        user2.setRole("nuser");  //普通用户
        List<User> users = new ArrayList<>();
        users.add(user);
        users.add(user2);
        map.put("users", users);
        map.put("user", user2);
        Integer[] ints = {12, 34, 234, 23};
        map.put("ints", ints);
        Map<String, User> userMap = new HashMap<>();
        userMap.put("11", user);
        userMap.put("22", user2);
        map.put("userMap", userMap);
//        存储当前日期时间
        map.put("curDate", new Date());
        request.getSession().setAttribute("username", user.getUsername());
        return "index.html";
    }

    @RequestMapping("/purchase")
    public String Purchase() {
        return "Purchase.html";
    }

    @GetMapping("/editor")
    public String editor() {
        return "editor.html";
    }


    @GetMapping("/Application")
    public String application() {
        return "Application.html";
    }
}
