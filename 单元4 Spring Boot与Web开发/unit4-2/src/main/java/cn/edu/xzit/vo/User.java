package cn.edu.xzit.vo;


import lombok.Data;

@Data
public class User {
    private String id;
    private String username;
    private String role;
}
