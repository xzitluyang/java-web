package cn.edu.xzit;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("cn.js.ccit.dao")
@SpringBootApplication

public class Unit42Application {

    public static void main(String[] args) {
        SpringApplication.run(Unit42Application.class, args);
    }

}
