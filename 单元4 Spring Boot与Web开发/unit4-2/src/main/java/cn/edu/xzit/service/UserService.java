package cn.edu.xzit.service;

import cn.edu.xzit.vo.User;

import java.util.List;

//服务层接口
public interface UserService {
    public List<User> getAll();
}
