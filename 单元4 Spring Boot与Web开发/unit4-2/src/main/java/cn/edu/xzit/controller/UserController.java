package cn.edu.xzit.controller;


import cn.edu.xzit.service.UserService;
import cn.edu.xzit.vo.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/getall")
    public String getAllUser(ModelMap map) {
        //存储获取到的用户信息，保存到map中
        List<User> users = userService.getAll();
        map.addAttribute("users", users);
        System.out.println(users);
        return "show.html";
    }

    @RequestMapping("/go")
    public String gohtml() {
        return "Stock.html";
    }
}
