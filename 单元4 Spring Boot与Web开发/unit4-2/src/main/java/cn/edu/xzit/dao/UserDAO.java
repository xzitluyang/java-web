package cn.edu.xzit.dao;

import cn.edu.xzit.vo.User;
import org.apache.ibatis.annotations.Mapper;


import java.util.List;

/**
 * UserDAO接口
 */
@Mapper
public interface UserDAO {
    public List<User> getAll();
}
