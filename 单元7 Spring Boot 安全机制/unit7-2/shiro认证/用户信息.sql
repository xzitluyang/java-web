-- 创建数据表：t_user（用户信息表）
DROP TABLE IF EXISTS t_user;

CREATE TABLE IF NOT EXISTS t_user
(
	id INT PRIMARY KEY COMMENT '用户ID（主键）',
	name VARCHAR(255) UNIQUE NOT NULL COMMENT '用户姓名',
	pwd VARCHAR(20) NOT NULL COMMENT '登录密码',
	perms VARCHAR(100) NOT NULL COMMENT '用户权限'
) COMMENT = '用户信息表';

INSERT INTO t_user(id,name,pwd,perms)
VALUE(1,'zhangsan','123','user:delete');
INSERT INTO t_user(id,name,pwd,perms)
VALUE(2,'lisi','123','user:add');

SELECT * FROM t_user