package cn.edu.xzit.service;

import cn.edu.xzit.pojo.User;

public interface UserService {
    User findUserByName(String uname);
}
