package cn.edu.xzit.service;

import cn.edu.xzit.mapper.UserMapper;
import cn.edu.xzit.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;

    @Override
    public User findUserByName(String uname) {
        return userMapper.findUserByName(uname);
    }
}
