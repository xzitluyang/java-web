package cn.edu.xzit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShiroauthenticationtestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShiroauthenticationtestApplication.class, args);
    }

}
