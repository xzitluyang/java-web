package cn.edu.xzit.mapper;

import cn.edu.xzit.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserMapper {
    User findUserByName(String uname);
}
