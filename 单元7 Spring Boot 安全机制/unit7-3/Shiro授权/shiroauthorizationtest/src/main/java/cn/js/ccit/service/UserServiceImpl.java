package cn.js.ccit.service;

import cn.js.ccit.mapper.UserMapper;
import cn.js.ccit.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;
    @Override
    public User findUserByName(String uname) {
        return userMapper.findUserByName(uname);
    }
}
