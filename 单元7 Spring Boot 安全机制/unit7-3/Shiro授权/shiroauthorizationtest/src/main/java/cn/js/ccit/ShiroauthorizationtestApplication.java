package cn.js.ccit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShiroauthorizationtestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShiroauthorizationtestApplication.class, args);
    }

}
