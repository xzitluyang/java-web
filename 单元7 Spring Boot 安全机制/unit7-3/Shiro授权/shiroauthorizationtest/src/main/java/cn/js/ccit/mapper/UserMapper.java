package cn.js.ccit.mapper;

import cn.js.ccit.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserMapper {
    public User findUserByName(String uname);
}
