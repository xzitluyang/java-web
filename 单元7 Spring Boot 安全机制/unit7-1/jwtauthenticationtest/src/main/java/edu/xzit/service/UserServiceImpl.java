package edu.xzit.service;

import edu.xzit.exception.RunException;
import edu.xzit.mapper.UserMapper;
import edu.xzit.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public User login(User user) {
        //根据接收用户名密码查询数据库
        User userDB = userMapper.login(user);

        if(userDB!=null){
            return userDB;
        }else{
            throw  new RunException("登录失败~~");
        }

    }
}
