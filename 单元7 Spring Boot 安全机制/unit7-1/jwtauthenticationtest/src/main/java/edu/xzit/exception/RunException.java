package edu.xzit.exception;


public class RunException extends RuntimeException {

    public RunException(String message) {
        super(message);
    }
}

