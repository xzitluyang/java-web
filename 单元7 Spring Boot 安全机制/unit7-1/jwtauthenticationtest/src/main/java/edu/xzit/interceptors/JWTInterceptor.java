package edu.xzit.interceptors;

import edu.xzit.exception.RunException;
import edu.xzit.utils.JWTUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * JWT拦截器，用于拦截请求并验证JWT令牌
 * 实现了HandlerInterceptor接口，以用于Spring MVC的拦截器机制
 */
public class JWTInterceptor implements HandlerInterceptor {

    /**
     * 在请求处理之前进行拦截处理
     *
     * @param request  请求对象，包含请求头、请求参数等信息
     * @param response 响应对象，用于向客户端返回数据
     * @param handler  处理请求的对象，可以用来判断是否为请求映射方法
     * @return boolean 返回值为true时放行请求，为false时中断请求处理
     * @throws Exception 可能抛出的异常
     */
    @Override
    public boolean preHandle(HttpServletRequest request, @Nonnull HttpServletResponse response, @Nonnull Object handler) throws Exception {
        System.out.println("进入了拦截器");
        // 创建一个Map对象用于存储返回给前端的数据
        Map<String, Object> map = new HashMap<>();
        // 获取请求头中令牌
        String token = request.getHeader("token");
        try {
            // 解析JWT令牌，验证其有效性
            Claims claims = JWTUtils.parseJwt(token);
            // 如果令牌有效，则放行请求
            System.out.println("通过了拦截器");
            return true;
        } catch (RunException e) {
            // 如果令牌解析失败，则捕获异常，并返回错误信息
            e.printStackTrace();
            //map.put("msg","密钥错误!");
            map.put("msg", e.getMessage());
        }
        // 设置状态为false，表示请求被拦截
        map.put("state", false);
        // 将map对象转换为JSON字符串，使用Jackson库进行序列化
        String json = new ObjectMapper().writeValueAsString(map);
        // 设置响应内容类型为JSON，并写入响应数据
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().println(json);
        // 返回false，中断请求处理
        return false;
    }
}

