package edu.xzit.config;

import edu.xzit.interceptors.JWTInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Override
    /*
     * 配置拦截器
     *
     * 该方法用于向应用程序添加拦截器，以在请求到达控制器之前执行某些逻辑
     * 主要用于需要在特定请求路径上进行JWT（JSON Web Token）验证的场景
     *
     * @param registry InterceptorRegistry的实例，用于注册拦截器
     */
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册JWTInterceptor拦截器，用于验证请求中的JWT token
        registry.addInterceptor(new JWTInterceptor())
                .addPathPatterns("/user/test")         // 其他接口token验证
                .excludePathPatterns("/user/login");  // 不进行token验证
    }
}
