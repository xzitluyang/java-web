package edu.xzit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtauthenticationtestApplication {

    public static void main(String[] args) {
        SpringApplication.run(JwtauthenticationtestApplication.class, args);
    }

}
