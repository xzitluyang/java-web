package com.springboot02amqp.amqp;

import com.rabbitmq.client.AMQP;
import com.springboot02amqp.amqp.bean.Book;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class AmqpApplicationTests {
    @Autowired
    RabbitTemplate rabbitTemplate;

    /*
    广播：发给对应的exchange.topic交换器就可以
     */
    @Test
    public void sendMsg()
    {
        rabbitTemplate.convertAndSend("exchange.topic","*.news",
                "这是一条采用通配符模式发送的消息，只有路由键以‘*.news’开头的队列才会收到");

    }

}
