package com.springboot02amqp.amqp.service;

import com.springboot02amqp.amqp.bean.Book;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class BookService {

    @RabbitListener(queues = "queue1.news")
    public void receive1(Object object)
    {
        System.out.println("receive1收到消息："+object.toString());
    }

    @RabbitListener(queues = "queue1.emps")
    public void receive2(Object o)
    {
        System.out.println("receive2收到消息："+o.toString());
    }
}
