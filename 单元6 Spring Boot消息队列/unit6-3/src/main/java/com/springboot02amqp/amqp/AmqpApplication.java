package com.springboot02amqp.amqp;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
/*
*自动配置
* 1、RabbitAutoConfiguration
* 2、有自动配置了连接工厂ConnecttionFactory；
* 3、RabbitProperties封装了RabbitMQ的配置
* 4、RabbitTemplate：给RabbitMQ发送和接受消息的
* 5、AmqpAdmin：RabbitMQ系统管理功能组件
*
* 6、@EnableRabbit+@RabbitListener监听消息队列的内容
 */
@EnableRabbit //开启基于注解的RabbitMQ模式
@SpringBootApplication
public class AmqpApplication {

    public static void main(String[] args) {
        SpringApplication.run(AmqpApplication.class, args);
    }

}
