package com.springboot02amqp.amqp;

import com.rabbitmq.client.AMQP;
import com.springboot02amqp.amqp.bean.Book;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class AmqpApplicationTests {
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Autowired
    AmqpAdmin amqpadmin;//创建一个AmqpAdmin对象amqpadmin
    @Test
    public void create()
    {
        //使用amqpadmin创建一个direct类型的交换器
        amqpadmin.declareExchange(new DirectExchange("amqpadmin.exchange"));
        //使用amqpadmin创建一个队列，两个参数（名字，受否是持久化）
        amqpadmin.declareQueue(new Queue("amqpadmin.queue",true));
        //使用amqpadmin绑定队列和交换器
        amqpadmin.declareBinding(new Binding("amqpadmin.queue", Binding.DestinationType.QUEUE,
                "amqpadmin.exchange","test.amqpadmin",null));
    }
    /*
   *1、单播（点对点）
     */
    @Test
    void contextLoads() {

        //对象被默认序列化发送出去
        rabbitTemplate.convertAndSend("exchange.direct","queue1.news",new Book("Java程序设计","Java教研室"));
    }
    //接收数据，如何将数据自动的转为json发送出去
    @Test
    public void receive()
    {
        Object o=rabbitTemplate.receiveAndConvert("queue2.news");
        System.out.println(o.getClass());
        System.out.println(o);
    }
    /*
    广播：更简单，发给对应的exchange交换器就可以
     */
    @Test
    public void sendMsg()
    {
        rabbitTemplate.convertAndSend("exchange.topic","1.news","这是一条采用通配符模式发送的消息，只有路由键以‘news’结尾的队列才会收到");

    }

}
