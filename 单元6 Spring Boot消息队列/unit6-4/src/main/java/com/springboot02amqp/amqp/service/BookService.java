package com.springboot02amqp.amqp.service;

import com.springboot02amqp.amqp.bean.Book;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class BookService {

    @RabbitListener(queues = "queue2.news")
    public void receive(Book book)
    {
        System.out.println("收到消息："+book);
    }
}
