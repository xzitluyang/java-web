package com.demo.library.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "book")   //实体类映射数据表
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    @Column
    private Double price;
    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date publishDate;

    @ManyToOne
    @JoinColumn(name = "publisherId")
    private Publisher publisher;

    public Publisher getPublisher() {
        return publisher;
    }



    public String getPublisherName() {
        if (publisher == null) {
            return null;
        }
        return publisher.getPublisherName();
    }

    @Column
    private String authorInfo;
}
