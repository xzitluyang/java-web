package com.demo.library.service;

import com.demo.library.entity.Book;

import java.util.List;

public interface BookService {
    List<Book> findAll();
    Book findById(Long id);
    void save(Book book);
    void update(Book book);
    void deleteById(Long id);
    List<Book> findByTitle(String title);
    String getPublisherNameById(Long id);

    List<String> findAuthorList();

}
