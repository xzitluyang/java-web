package com.demo.library.config;

import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    public static final String exchange = "libraryExchange";

    public static final String queue = "libraryQueue";

    public static final String routingKey = "library.log";

}
