package com.demo.library.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "publisher")
@Data
public class Publisher implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "publisherName")
    private String publisherName;

    public String getPublisherName() {
        return publisherName;
    }

}
