package com.demo.library.Dao;

import com.demo.library.entity.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PublisherRepository  extends JpaRepository<Publisher, Integer> {
    List<Publisher> findByPublisherNameContains(String publisherName);
}
