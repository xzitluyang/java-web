package com.demo.library.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "log")
public class Log implements Serializable {

    private static final long serializableID = 10L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "info")
    private String Info;

    @Column(name = "createtime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createTime;


}
