package com.demo.library.controller;

import com.demo.library.entity.Publisher;
import com.demo.library.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/publisher")
public class PublisherController {

    @Autowired
    private PublisherService publisherService;

    @GetMapping("/publisherList")
    public ModelAndView publisherList() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("publisherList");
        modelAndView.addObject("publishers", publisherService.findAll());
        return modelAndView;
    }

    @GetMapping("/findById/{id}")
    public ModelAndView findById(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("PublisherEditor");
        modelAndView.addObject("publisher", publisherService.getById(id));
        return modelAndView;
    }

    @PostMapping("/save")
    public String save(Publisher publisher) {
        publisherService.save(publisher);
        return "redirect:/publisher/publisherList";
    }

    @GetMapping("/deleteById/{id}")
    public String delete(@PathVariable Integer id) {
        publisherService.deleteById(id);
        return "redirect:/publisher/publisherList";
    }

    @PostMapping("/update")
    public String update(Publisher publisher) {
        publisherService.update(publisher);
        return "redirect:/publisher/publisherList";
    }

    @PostMapping("/search")
    public ModelAndView search(@RequestParam String publisherName) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("publisherList");
        modelAndView.addObject("publishers", publisherService.findByPublisherNameContains(publisherName));
        return modelAndView;
    }
}
