package com.demo.library.service.Impl;

import com.demo.library.Dao.PublisherRepository;
import com.demo.library.entity.Publisher;
import com.demo.library.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class PublisherServiceImpl implements PublisherService {

    @Autowired
    private PublisherRepository publisherRepository;

    @Override
    public List<Publisher> findAll() {
        return publisherRepository.findAll();
    }

    @Override
    public Publisher getById(Integer id) {
        return publisherRepository.getById(id);
    }

    @Override
    public void save(Publisher publisher) {
        publisherRepository.save(publisher);
    }

    @Override
    public void update(Publisher publisher) {
        publisherRepository.save(publisher);
    }

    @Override
    public void deleteById(Integer id) {
        publisherRepository.deleteById(id);
    }

    @Override
    public List<Publisher> findByPublisherNameContains(String publisherName) {
        return publisherRepository.findByPublisherNameContains(publisherName.trim());
    }
}
