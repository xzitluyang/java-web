# 基于 spring boot + JPA + thymeleaf + RabbitMQ 的图书管理功能案例

**一、项目准备**

1. 创建 Spring Boot 项目

   - 使用 IDEA创建一个新的 Spring Boot 项目，将包路径设置为 `com.demo.library` 。
   - 项目创建时选择依赖项：Lombok，Spring Boot DevTools，Spring Web，MySql Driver
   - 安装rabbitMQ，创建交换器libraryExchange，选择direct模式，队列libraryQueue，使用library.log作为路由键值，绑定交换器和队列
2. 添加依赖
   - 在 `pom.xml` 文件中添加bootStrap、JQuery、JPA、thymeleaf相关依赖：

```xml
<dependencies>
   <dependency>
      <groupId>org.webjars</groupId>
      <artifactId>bootstrap</artifactId>
      <version>3.3.7</version>
   </dependency>
   <dependency>
      <groupId>org.webjars</groupId>
      <artifactId>jquery</artifactId>
      <version>3.1.1</version>
   </dependency>
   <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-jpa</artifactId>
   </dependency>
   <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-thymeleaf</artifactId>
   </dependency>
   <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-amqp</artifactId>
   </dependency>
</dependencies>

```

```
# application.yml 配置文件内容
spring:
   datasource:
     url: jdbc:mysql://localhost:3306/library?useSSL=false&serverTimezone=UTC
     username: root
     password: root
     driver-class-name: com.mysql.cj.jdbc.Driver
   
   jpa:
     hibernate:
       ddl-auto: update
       naming:
         physical-strategy: org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl
     show-sql: true
   
   thymeleaf:
     prefix: classpath:/templates/
     suffix: .html
     cache: false
     encoding: UTF-8
     servlet:
       context-path: /book/bookList
   
   rabbitmq:
     host: localhost
     port: 5672
     username: guest
     password: guest

```

**二、数据库设计**

1. 创建数据库
   创建名为 library 的数据库。


**三、实体类创建**

- 创建配置文件RabbitMQConfig
```java
 @Configuration
public class RabbitMQConfig {

   public static final String exchange = "libraryExchange";

   public static final String queue = "libraryQueue";

   public static final String routingKey = "library.log";

}

```
 

- 创建 Book 实体类

```java

package com.demo.library.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "book")   //实体类映射数据表
public class Book {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;
   private String title;
   @Column
   private Double price;
   @Column
   @DateTimeFormat(pattern = "yyyy-MM-dd")
   private Date publishDate;

   //设置外键关联
   @ManyToOne
   @JoinColumn(name = "publisherId")
   private Publisher publisher;

   public Publisher getPublisher() {
      return publisher;
   }

   public String getPublisherName() {
      if (publisher == null) {
         return null;
      }
      return publisher.getPublisherName();
   }

   @Column
   private String authorInfo;
}

```

- 创建 Publisher 实体类

```java

package com.demo.library.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "publisher")
@Data
public class Publisher implements Serializable {
   private static final long serialVersionUID = 1L;

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;

   @Column(name = "publisherName")
   private String publisherName;

   public String getPublisherName() {
      return publisherName;
   }

}

```

- 创建Log日志实体类

```java
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "log")
public class Log implements Serializable {

    private static final long serializableID = 10L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "info")
    private String Info;

    @Column(name = "createtime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

}
```



**四、Mapper 接口创建**

- 创建 BookDAO 接口

```java
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

   List<Book> findByTitleContains(String title);

   @Query(value = "select authorInfo from book",nativeQuery = true)
   List<String> getAuthorList();
}

```

- 创建 PublisherDAO 接口

```java

@Repository
public interface PublisherRepository  extends JpaRepository<Publisher, Integer> {
   List<Publisher> findByPublisherNameContains(String publisherName);
}
```

- 创建LogDAO接口
```java
@Repository
public interface LogRepository extends JpaRepository<Log,Long> {}
```


**五、服务层创建**

- 创建 BookService 接口

```java
public interface BookService {
   List<Book> findAll();
   Book findById(Long id);
   void save(Book book);
   void update(Book book);
   void deleteById(Long id);
   List<Book> findByTitle(String title);
   String getPublisherNameById(Long id);
   List<String> findAuthorList();
}
```

- 创建 BookServiceImpl 实现类

```java
@Service
public class BookServiceImpl implements BookService {

   private final BookRepository bookRepository;

   private final LogRepository logRepository;

   private final RabbitTemplate rabbitTemplate;

   public BookServiceImpl(BookRepository bookRepository, LogRepository logRepository, RabbitTemplate rabbitTemplate) {
      this.bookRepository = bookRepository;
      this.logRepository = logRepository;
      this.rabbitTemplate = rabbitTemplate;
   }

   @Override
   public List<Book> findAll() {
      return bookRepository.findAll();
   }

   @Override
   public Book findById(Long id) {
      return bookRepository.getById(id);
   }

   @Override
   public void save(Book book) {
      Log log = new Log();
      log.setInfo("添加书籍");
      log.setCreateTime(new Date());
      rabbitTemplate.convertAndSend(RabbitMQConfig.exchange, RabbitMQConfig.routingKey, log);
      bookRepository.save(book);

   }

   @Override
   public void update(Book book) {
      Log log = new Log();
      log.setInfo("修改书籍");
      log.setCreateTime(new Date());
      rabbitTemplate.convertAndSend(RabbitMQConfig.exchange, RabbitMQConfig.routingKey, log);
      bookRepository.save(book);

   }

   @Override
   public void deleteById(Long id) {

      bookRepository.deleteById(id);
   }

   @Override
   public List<Book> findByTitle(String title) {
      return bookRepository.findByTitleContains(title.trim());
   }

   @Override
   public String getPublisherNameById(Long id) {
      Book book = bookRepository.getById(id);
      if (book != null) {
         return book.getPublisher().getPublisherName();
      }
      return null;
   }

   @Override
   public List<String> findAuthorList() {
      return bookRepository.getAuthorList();
   }

   @RabbitListener(queues = RabbitMQConfig.queue)
   public void receiveLog(Log log) {
      logRepository.save(log);
      System.out.println("接收到的日志信息：" + log);
   }

}

```

- 创建 PublisherService 接口

```java
public interface PublisherService {
   List<Publisher> findAll();
   Publisher getById(Integer id);
   void save(Publisher publisher);
   void update(Publisher publisher);
   void deleteById(Integer id);
   List<Publisher> findByPublisherNameContains(String publisherName);

}
```

- 创建 PublisherServiceImpl 实现类

```java
@Service
public class PublisherServiceImpl implements PublisherService {

   @Autowired
   private PublisherRepository publisherRepository;

   @Override
   public List<Publisher> findAll() {
      return publisherRepository.findAll();
   }

   @Override
   public Publisher getById(Integer id) {
      return publisherRepository.getById(id);
   }

   @Override
   public void save(Publisher publisher) {
      publisherRepository.save(publisher);
   }

   @Override
   public void update(Publisher publisher) {
      publisherRepository.save(publisher);
   }

   @Override
   public void deleteById(Integer id) {
      publisherRepository.deleteById(id);
   }

   @Override
   public List<Publisher> findByPublisherNameContains(String publisherName) {
      return publisherRepository.findByPublisherNameContains(publisherName.trim());
   }
}
```

**六、控制器层创建**

- 创建 BookController 控制器
```java

@Controller
@RequestMapping("/book")
public class BookController {

   @Autowired
   private BookService bookService;

   @Autowired
   private PublisherService publisherService;

   @GetMapping("/bookList")
   public ModelAndView bookList() {
      ModelAndView modelAndView = new ModelAndView();
      modelAndView.setViewName("bookList");
      modelAndView.addObject("books", bookService.findAll());
      modelAndView.addObject("publishers", publisherService.findAll());
      return modelAndView;
   }

   @GetMapping("/findById/{id}")
   public ModelAndView findById(@PathVariable Long id) {
      ModelAndView modelAndView = new ModelAndView();
      modelAndView.addObject("book", bookService.findById(id));
      modelAndView.setViewName("BookEditor");
      modelAndView.addObject("publishers", publisherService.findAll());
      return modelAndView;
   }

   @PostMapping("/save")
   public String save(Book book, @RequestParam Integer publisherId) {
      book.setPublisher(publisherService.getById(publisherId));
      bookService.save(book);
      return "redirect:/book/bookList";
   }

   //根据id删除数据库的记录
   @GetMapping("/delete/{id}")
   public String deleteById(@PathVariable Long id) {
      bookService.deleteById(id);
      return "redirect:/book/bookList";
   }

   //根据id修改数据记录
   @PostMapping("/update")
   public String updateById(Book book, @RequestParam Integer publisherId) {
      book.setPublisher(publisherService.getById(publisherId));
      bookService.update(book);
      return "redirect:/book/bookList";
   }

   @GetMapping("/SearchList")
   public ModelAndView SearchList(@RequestParam String searchName) {
      ModelAndView modelAndView = new ModelAndView();
      modelAndView.setViewName("bookList");
      modelAndView.addObject("books", bookService.findByTitle(searchName));
      return modelAndView;
   }

   @GetMapping("/AuthorList")
   public ModelAndView AuthorList() {
      ModelAndView modelAndView = new ModelAndView();
      modelAndView.setViewName("authorList");
      modelAndView.addObject("authors", bookService.findAuthorList());
      return modelAndView;
   }
}

```

- 创建 PublisherController 控制器

```java
@Controller
@RequestMapping("/publisher")
public class PublisherController {

   @Autowired
   private PublisherService publisherService;

   @GetMapping("/publisherList")
   public ModelAndView publisherList() {
      ModelAndView modelAndView = new ModelAndView();
      modelAndView.setViewName("publisherList");
      modelAndView.addObject("publishers", publisherService.findAll());
      return modelAndView;
   }

   @GetMapping("/findById/{id}")
   public ModelAndView findById(@PathVariable Integer id) {
      ModelAndView modelAndView = new ModelAndView();
      modelAndView.setViewName("PublisherEditor");
      modelAndView.addObject("publisher", publisherService.getById(id));
      return modelAndView;
   }

   @PostMapping("/save")
   public String save(Publisher publisher) {
      publisherService.save(publisher);
      return "redirect:/publisher/publisherList";
   }

   @GetMapping("/deleteById/{id}")
   public String delete(@PathVariable Integer id) {
      publisherService.deleteById(id);
      return "redirect:/publisher/publisherList";
   }

   @PostMapping("/update")
   public String update(Publisher publisher) {
      publisherService.update(publisher);
      return "redirect:/publisher/publisherList";
   }

   @PostMapping("/search")
   public ModelAndView search(@RequestParam String publisherName) {
      ModelAndView modelAndView = new ModelAndView();
      modelAndView.setViewName("publisherList");
      modelAndView.addObject("publishers", publisherService.findByPublisherNameContains(publisherName));
      return modelAndView;
   }
}

```

**七、视图层**
在 resources/templates 目录下创建以下页面：
-  authorList.html

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.w3.org/1999/xhtml">
<html xmlns:th="http://www.thymeleaf.org"></html>
<head>
    <meta charset="UTF-8">
    <script src="/webjars/jquery/3.1.1/jquery.min.js"></script>
    <script src="/webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <title>图书管理系统</title>
</head>
<body>
<!-- 导航栏 -->
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" datatoggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" ariaexpanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/book/bookList">图书管理系统</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/book/bookList">图书信息<span class="sr-only">
    (current)</span></a></li>
                <li><a href="#">新书推荐</a></li>

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                       role="button" aria-haspopup="true" aria-expanded="false">用户管理 <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">修改密码</a></li>
                        <li><a href="#">添加地址</a></li>
                        <li><a href="#">订单记录</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">退出登录</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- 布局 3：9-->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
                <a href="#" class="list-group-item active">
                    首页
                </a>
                <a href="/book/bookList" class="list-group-item">图书列表</a>
                <a href="/publisher/publisherList" class="list-group-item">出版社列表</a>
                <a href="/book/AuthorList" class="list-group-item">作者信息列表</a>
                <a href="#" class="list-group-item">关于我们</a>
            </div>
        </div>

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">作者信息列表</div>

                <table class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>姓名</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr th:each="author,iterStat:${authors}">
                        <td th:text="${iterStat.index+1}"></td>
                        <td th:text="${author}"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">添加新书籍</h4>
            </div>
            <div class="modal-body">
                <form th:action="@{/book/save}" method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="title" class="col-md-2 control-label">图书标题
                        </label>
                        <div class="col-md-8">
                            <input type="text" id="title" name="title" class="form-control"
                                   placeholder="请输入图书标题...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="price" class="col-md-2 control-label">图书价格
                        </label>
                        <div class="col-md-8">
                            <input type="text" id="price" name="price" class="form-control"
                                   placeholder="请输入图书价格...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="date" class="col-md-2 control-label">出版日期
                        </label>
                        <div class="col-md-8">
                            <input type="date" id="date" name="publishDate" class="form-control"
                                   placeholder="请输入出版日期...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="publish" class="col-md-2 control-label">出版社名
                        </label>
                        <div class="col-md-8">
                            <select id="publish" name="publisherId" class="form-control">
                                <option th:each="p:${publishers}" th:text="${p.publisherName}" th:value="${p.id}"></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="author" class="col-md-2 control-label">图书作者
                        </label>
                        <div class="col-md-8">
                            <input type="text" id="author" name="authorInfo" class="form-control"
                                   placeholder="请输入图书作者...">
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>

                <button type="button" class="btn btn-success" id="submit_btn">保存提交</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    //绑定按钮的点击事件
    $("#btn").click(function () {
        // 手动打开模态框
        $("#myModal").modal('show');
    });
    //手动关闭模态框
    $("#submit_btn").click(function () {
        $("form").submit();
        $('#myModal').modal('hide');
    });
</script>
</body>
</html>

```
-  bookList.html
```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.w3.org/1999/xhtml">
<html xmlns:th="http://www.thymeleaf.org"></html>
<head>
    <meta charset="UTF-8">
    <script src="/webjars/jquery/3.1.1/jquery.min.js"></script>
    <script src="/webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <title>图书管理系统</title>
</head>
<body>
<!-- 导航栏 -->
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" datatoggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" ariaexpanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/book/bookList">图书管理系统</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/book/bookList">图书信息<span class="sr-only">
    (current)</span></a></li>
                <li><a href="#">新书推荐</a></li>

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                       role="button" aria-haspopup="true" aria-expanded="false">用户管理 <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">修改密码</a></li>
                        <li><a href="#">添加地址</a></li>
                        <li><a href="#">订单记录</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">退出登录</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- 布局 3：9-->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
                <a href="#" class="list-group-item active">
                    首页
                </a>
                <a href="/book/bookList" class="list-group-item">图书列表</a>
                <a href="/publisher/publisherList" class="list-group-item">出版社列表</a>
                <a href="/book/AuthorList" class="list-group-item">作者信息列表</a>
                <a href="#" class="list-group-item">关于我们</a>
            </div>
        </div>

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">书籍信息列表</div>
                <form class="navbar-form navbar-left" style="margin-bottom: 20px;" action="/book/SearchList">
                    <div class="form-group">
                        <button class="btn btn-success" type="button" data-toggle="modal" id="btn">添加新书</button>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="searchName" placeholder="搜素更多图书...">
                    </div>
                    <button type="submit" class="btn btn-primary">搜索</button>
                </form>
                <table class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>书籍名称</th>
                        <th>价格</th>
                        <th>出版日期</th>
                        <th>图书出版社</th>
                        <th>作者信息</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr th:each="book:${books}">
                        <td th:text="${book.id}"></td>
                        <td th:text="${book.title}"></td>
                        <td th:text="${book.price}"></td>
                        <td th:text="${#dates.format(book.publishDate,'yyyy-MM-dd')}"></td>
                        <td th:text="${book.publisherName}"></td>
                        <td th:text="${book.authorInfo}"></td>
                        <td>
                            <a th:href="@{/book/findById/{cid}(cid=${book.id})}" class="btn btn-primary">编辑</a>
                            <a th:href="@{/book/delete/{id}(id=${book.id})}" class="btn btn-danger">删除</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">添加新书籍</h4>
            </div>
            <div class="modal-body">
                <form th:action="@{/book/save}" method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="title" class="col-md-2 control-label">图书标题
                        </label>
                        <div class="col-md-8">
                            <input type="text" id="title" name="title" class="form-control"
                                   placeholder="请输入图书标题...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="price" class="col-md-2 control-label">图书价格
                        </label>
                        <div class="col-md-8">
                            <input type="text" id="price" name="price" class="form-control"
                                   placeholder="请输入图书价格...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="date" class="col-md-2 control-label">出版日期
                        </label>
                        <div class="col-md-8">
                            <input type="date" id="date" name="publishDate" class="form-control"
                                   placeholder="请输入出版日期...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="publish" class="col-md-2 control-label">出版社名
                        </label>
                        <div class="col-md-8">
                            <select id="publish" name="publisherId" class="form-control">
                                <option th:each="p:${publishers}" th:text="${p.publisherName}" th:value="${p.id}"></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="author" class="col-md-2 control-label">图书作者
                        </label>
                        <div class="col-md-8">
                            <input type="text" id="author" name="authorInfo" class="form-control"
                                   placeholder="请输入图书作者...">
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>

                <button type="button" class="btn btn-success" id="submit_btn">保存提交</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    //绑定按钮的点击事件
    $("#btn").click(function () {
        // 手动打开模态框
        $("#myModal").modal('show');
    });
    //手动关闭模态框
    $("#submit_btn").click(function () {
        $("form").submit();
        $('#myModal').modal('hide');
    });
</script>
</body>
</html>

```

-  BookEditor.html
```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.w3.org/1999/xhtml">
<html xmlns:th="http://www.thymeleaf.org"></html>
<head>
    <meta charset="UTF-8">
    <script src="/webjars/jquery/3.1.1/jquery.min.js"></script>
    <script src="/webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <title>修改图书信息</title>
</head>
<body>
<!-- 导航栏 -->
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" datatoggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" ariaexpanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/book/bookList">图书管理系统</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">图书信息<span class="sr-only">
    (current)</span></a></li>
                <li><a href="#">新书推荐</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                       role="button" aria-haspopup="true" aria-expanded="false">更多操作 <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="搜素更多图书...">
                </div>
                <button type="submit" class="btn btn-default">搜索</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Training.L</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                       role="button" aria-haspopup="true" aria-expanded="false">用户管理 <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">修改密码</a></li>
                        <li><a href="#">添加地址</a></li>
                        <li><a href="#">订单记录</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">退出登录</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- 布局 3：7 -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
                <a href="#" class="list-group-item active">
                    首页
                </a>
                <a href="/book/bookList" class="list-group-item">图书列表</a>
                <a href="/publisher/publisherList" class="list-group-item">出版社列表</a>
                <a href="/book/AuthorList" class="list-group-item">作者信息列表</a>
                <a href="#" class="list-group-item">关于我们</a>
            </div>
        </div>
        <div class="col-md-9">
            <h1 class="text-center text-muted">更新图书信息</h1>
            <hr>
            <form th:action="@{/book/update}" method="post" class="form-horizontal" role="form">
                <div class="form-group">
                    <label for="ID" class="col-sm-2 control-label">图书编号:</label>
                    <div class="col-sm-10">
                        <input type="text" name="id" id="ID" class="form-control"
                               th:value="${book.id}" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">书籍名称:</label>
                    <div class="col-sm-10">
                        <input type="text" name="title" id="title" class="form-control"
                               th:value="${book.title}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="price" class="col-sm-2 control-label">书籍价格:</label>
                    <div class="col-sm-10">
                        <input type="text" name="price" id="price" class="form-control"
                               th:value="${book.price}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="date" class="col-sm-2 control-label">出版日期:</label>
                    <div class="col-sm-10">
                        <input type="date" name="publishDate" id="date" class="form-control"
                               th:value="${#dates.format(book.publishDate,'yyyy-MM-dd')}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">图书出版社:</label>
                    <div class="col-sm-10">
                        <select name="publisherId" class="form-control">
                            <option th:each="p:${publishers}" th:text="${p.publisherName}" th:value="${p.id}"></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="author" class="col-sm-2 control-label">作者:</label>
                    <div class="col-sm-10">
                        <input type="text" name="authorInfo" id="author" class="form-control"
                               th:value="${book.authorInfo}">
                    </div>
                </div>
                <hr>
                <input type="submit" value="提交修改" class="btn btn-primary btn-block">
            </form>
        </div>
    </div>
</div>
</body>
</html>

```

-  publisherList.html
```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.w3.org/1999/xhtml">
<html xmlns:th="http://www.thymeleaf.org"></html>
<head>
    <meta charset="UTF-8">
    <script src="/webjars/jquery/3.1.1/jquery.min.js"></script>
    <script src="/webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <title>图书管理系统</title>
</head>
<body>
<!-- 导航栏 -->
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" datatoggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" ariaexpanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/book/bookList">图书管理系统</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/book/bookList">图书信息<span class="sr-only">
    (current)</span></a></li>
                <li><a href="#">新书推荐</a></li>

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                       role="button" aria-haspopup="true" aria-expanded="false">用户管理 <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">修改密码</a></li>
                        <li><a href="#">添加地址</a></li>
                        <li><a href="#">订单记录</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">退出登录</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- 布局 3：7 -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
                <a href="#" class="list-group-item active">
                    首页
                </a>
                <a href="/book/bookList" class="list-group-item">图书列表</a>
                <a href="/publisher/publisherList" class="list-group-item">出版社列表</a>
                <a href="/book/AuthorList" class="list-group-item">作者信息列表</a>
                <a href="#" class="list-group-item">关于我们</a>
            </div>
        </div>

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">出版社信息列表</div>
                <form class="navbar-form navbar-left" style="margin-bottom: 20px;" action="/publisher/search">
                    <div class="form-group">
                        <button class="btn btn-success" type="button" data-toggle="modal" id="btnAdd">添加出版社
                        </button>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="searchName" placeholder="搜素更多出版社...">
                    </div>
                    <button type="submit" class="btn btn-primary">搜索</button>
                </form>
                <table class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>出版社名称</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr th:each="publisher:${publishers}">
                        <td th:text="${publisher.id}"></td>
                        <td th:text="${publisher.publisherName}"></td>
                        <td>
                            <a th:href="@{/publisher/findById/{id}(id=${publisher.id})}"
                               class="btn btn-primary">编辑</a>
                            <a th:href="@{/publisher/deleteById/{id}(id=${publisher.id})}"
                               class="btn btn-danger">删除</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
        <form th:action="@{/publisher/save}" method="post" class="form-horizontal" role="form">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">添加出版社信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="publisherName" class="col-md-2 control-label">出版社名称
                        </label>
                        <div class="col-md-8">
                            <input type="text" id="publisherName" name="publisherName" class="form-control"
                                   placeholder="请输入出版社名称">
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>

                    <button type="button" class="btn btn-success" id="submit_btn">保存提交</button>
                </div>

            </div>
        </form>
    </div>
</div>


<script type="text/javascript">
    //绑定按钮的点击事件
    $("#btnAdd").click(function () {
        // 手动打开模态框
        $("#myModal").modal('show');
    });
    //手动关闭模态框
    $("#submit_btn").click(function () {
        $("form").submit();
        $('#myModal').modal('hide');
    });
</script>
</body>
</html>

```

-  PublisherEditor.html
```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.w3.org/1999/xhtml">
<html xmlns:th="http://www.thymeleaf.org"></html>
<head>
    <meta charset="UTF-8">
    <script src="/webjars/jquery/3.1.1/jquery.min.js"></script>
    <script src="/webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <title>修改出版社信息</title>
</head>
<body>
<!-- 导航栏 -->
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" datatoggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" ariaexpanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/book/bookList">图书管理系统</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">图书信息<span class="sr-only">
    (current)</span></a></li>
                <li><a href="#">新书推荐</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                       role="button" aria-haspopup="true" aria-expanded="false">更多操作 <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="搜素更多图书...">
                </div>
                <button type="submit" class="btn btn-default">搜索</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Training.L</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                       role="button" aria-haspopup="true" aria-expanded="false">用户管理 <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">修改密码</a></li>
                        <li><a href="#">添加地址</a></li>
                        <li><a href="#">订单记录</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">退出登录</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- 布局 3：7 -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
                <a href="#" class="list-group-item active">
                    首页
                </a>
                <a href="/book/bookList" class="list-group-item">图书列表</a>
                <a href="/publisher/publisherList/" class="list-group-item">出版社列表</a>
                <a href="/book/AuthorList" class="list-group-item">作者信息列表</a>
                <a href="#" class="list-group-item">关于我们</a>
            </div>
        </div>
        <div class="col-md-9">
            <h1 class="text-center text-muted">更新出版社信息</h1>
            <hr>
            <form th:action="@{/publisher/update}" method="post" class="form-horizontal" role="form">
                <div class="form-group">
                    <label for="ID" class="col-sm-2 control-label">出版社编号:</label>
                    <div class="col-sm-10">
                        <input type="text" name="id" id="ID" class="form-control"
                               th:value="${publisher.id}" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="publisherName" class="col-sm-2 control-label">出版社名称:</label>
                    <div class="col-sm-10">
                        <input type="text" name="publisherName" id="publisherName" class="form-control"
                               th:value="${publisher.publisherName}">
                    </div>
                </div>

                <hr>
                <input type="submit" value="提交修改" class="btn btn-primary btn-block">
            </form>
        </div>
    </div>
</div>
</body>
</html>

```

**八、运行项目**

运行项目，访问 http://localhost:8080/book/bookList 查看图书信息列表。

添加书籍、修改书籍，查看log表中的日志信息，尝试为log日志单独编写一个展示页面。




