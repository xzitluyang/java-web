package com.springboot02amqp.amqp;

import com.springboot02amqp.amqp.bean.Book;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class AmqpApplicationTests {
    @Autowired
    RabbitTemplate rabbitTemplate;

    private String queue="queue1";
    /*
     *1、单播（点对点）
     */
    @Test
    void contextLoads() {
//        send();
//        receive();
        //对象被默认序列化发送出去
        rabbitTemplate.convertAndSend("exchange.direct", "queue1.news", new Book("Java程序设计", "Java教研室"));
    }

    @Test
    void send(){
        Map<String,Object> map=new HashMap<>();
        map.put("msg","这是第一个消息");
        map.put("data", Arrays.asList("helloworld",123,true));
        rabbitTemplate.convertAndSend("exchange.fanout","",map);
        System.out.println("发送消息成功");
    }

    //接收数据，如何将数据自动的转为json发送出去
    @Test
    void receive() {
        System.out.println(new String(rabbitTemplate.receive("queue1.news").getBody()));
    }


    @Test
    public void sendAndReceiveMessage() {
        // 发送消息
//        String message = "Test message";
//        rabbitTemplate.convertAndSend(queue, message);

        // 接收消息
        Message receivedMessage = rabbitTemplate.receive("queue3");
        if (receivedMessage!= null) {
            String receivedText = new String(receivedMessage.getBody());
            System.out.println("Received message: " + receivedText);
        } else {
            System.out.println("No message received");
        }
    }


}
