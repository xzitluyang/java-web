package com.springboot02amqp.amqp.service;

import com.springboot02amqp.amqp.bean.Book;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class BookService {

    @RabbitListener(queues = "queue1")
    public void receive1(Object object) throws InterruptedException {
        Thread.sleep(1000);
        System.out.println("receive1收到消息："+object.toString());
    }


    @RabbitListener(queues = "queue2")
    public void receive2(Object object) throws InterruptedException {
        Thread.sleep(2000);

        System.out.println("receive2收到消息："+object.toString());
    }

    @RabbitListener(queues = "queue3")
    public void receive3(Object object) throws InterruptedException {
        Thread.sleep(3000);
        System.out.println("receive3收到消息："+object.toString());
    }

}
