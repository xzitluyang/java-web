package com.springboot02amqp.amqp;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AmqpApplicationTests {
    @Autowired
    RabbitTemplate rabbitTemplate;

    /*
    广播：更简单，发给对应的exchange交换器就可以
     */
    @Test
    public void sendMsg() {
        rabbitTemplate.convertAndSend("exchange.fanout", "", "这是广播的一条消息");

    }
}
