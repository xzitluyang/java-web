package cn.js.ccit.quartz;

import cn.js.ccit.jobs.HelloJob;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;


public class TriggerRunner {
    public static void main(String[] args) throws SchedulerException {
        // JobDetail：用来绑定Job，并且在job执行的时候携带一些执行的信息
        //创建一个JobDetail实例，将该实例与HelloJob Class绑定
        JobDetail jobDetail = JobBuilder.newJob(HelloJob.class)
                //指定Job的名字
                .withIdentity("myJob","group1")
                //存储用户名
                .usingJobData("username","张三")
                .build();
        //存储数据
        JobDataMap jobDataMap=jobDetail.getJobDataMap();
        jobDataMap.put("depart","销售部");

        //Trigger：用来触发job去执行的，包括定义了什么时候去执行，第一次执行，是否会一直重复地执行下去，执行几次等
        //创建一个Trigger实例，定义该job立即执行，并且每隔2秒钟重复执行一次，重复三次
        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("myTrigger","group1")
                .startNow()
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        //使用SimpleSchedule，每2秒调度一次，执行三次
                        .withIntervalInSeconds(2).withRepeatCount(3))
//                .withSchedule(CronScheduleBuilder.cronSchedule("0 0 9 ? * MON"))
                .build();
        //创建scheduler实例：scheduler区别于trigger和jobDetail，是通过factory模式创建的
        //创建一个ScheduleFactory
        SchedulerFactory schedulerFactory = new StdSchedulerFactory();
        //创建一个调度器Scheduler
        Scheduler scheduler = schedulerFactory.getScheduler();
        //注入jobDetail和trigger到调度器中
        scheduler.scheduleJob(jobDetail,trigger);
        //开始调度
        scheduler.start();
    }
}
