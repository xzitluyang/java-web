package cn.js.ccit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Unit82Application {

    public static void main(String[] args) {
        SpringApplication.run(Unit82Application.class, args);
    }

}
