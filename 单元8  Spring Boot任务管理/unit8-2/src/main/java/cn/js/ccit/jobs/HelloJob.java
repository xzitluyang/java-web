package cn.js.ccit.jobs;

import org.quartz.*;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 定时任务类，描述需要定时执行的具体业务逻辑
 * @author 陆杨
 */
public class HelloJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        //获取JobDetail对象
        JobDetail jobDetail = context.getJobDetail();
        //获取JobDataMap对象
        JobDataMap jobDataMap = jobDetail.getJobDataMap();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss E");
        System.out.println(jobDataMap.get("depart") + "-----" + jobDataMap.get("username") + "现在时间是:" + sf.format(new Date()) + ",新的一周开始了！");
    }
}
