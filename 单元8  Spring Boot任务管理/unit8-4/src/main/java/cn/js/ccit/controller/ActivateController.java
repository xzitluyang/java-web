package cn.js.ccit.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ActivateController {
    @RequestMapping("/active/{rand}")
    public String active(@PathVariable("rand")double rand){
        return "激活成功";
    }
}
