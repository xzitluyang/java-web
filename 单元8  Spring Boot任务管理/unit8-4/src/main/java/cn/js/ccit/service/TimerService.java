package cn.js.ccit.service;

import org.quartz.JobDetail;
import org.springframework.cglib.core.internal.LoadingCache;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

//scheduler定时器执行任务的类
@Service
public class TimerService {

    //延迟3秒
//    @Scheduled(fixedDelay = 3000)
//    @Scheduled(fixedDelayString = "5000")
//    public void configureTasks2() {
//        System.err.println("3秒后，执行定时任务 " + LocalDateTime.now()+" "+Thread.currentThread().getName());
//    }
//    //或直接指定时间间隔，例如：5秒
//    @Scheduled(fixedRate=5000)
    @Async
    @Scheduled(fixedRateString = "5000", initialDelay = 7000)
    public void configureTasks() {
        System.err.println("执行静态定时任务时间: " + LocalDateTime.now() + " " + Thread.currentThread().getName());
    }

    //每隔5秒打印输出信息
    @Async
    @Scheduled(cron = "0/5 * * * * 0-7")
    public void hello() {
        System.out.println("hello....." + LocalDateTime.now()+" "+Thread.currentThread().getName());
    }
    //读取配置文件中的cron表达式值
//    @Scheduled(cron = "${time.cron}")
//    public void timerTask() {
//        System.out.println("读到配置文件中的cron表达式值：" + LocalDateTime.now());
//    }
}
