package cn.js.ccit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@SpringBootTest
class Unit84ApplicationTests {
    @Autowired
    JavaMailSenderImpl mailSender;


    /**
     * 发送文本邮件
     */
    @Test
    void sendText() {
        //定制发送的简单文本邮件信息
        SimpleMailMessage message = new SimpleMailMessage();
        //发送邮件的邮箱
        message.setFrom("114056405@qq.com");
        //接收邮件的邮箱
        message.setTo("lvfeng@ccit.js.cn");
        //邮件主题
        message.setSubject("简单文本邮件");
        //邮件内容
        message.setText("测试邮件内容");
        mailSender.send(message);
    }

    //发送复杂邮件
    @Test
    public void sendComplex()throws MessagingException{
        //定制发送的复杂邮件信息
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        //邮件配置的辅助工具类，multipart为true,表示多个部件
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true);
        helper.setFrom("114056405@qq.com");
        helper.setTo("lvfeng@ccit.js.cn");
        helper.setSubject("发送复杂邮件");
        //发送的文本中带有html标签，会进行解析
        String t1="<html><body><h1>这是邮件的标题</h1><br>";
        //设置内嵌的图片，图片源来自于本地，通过cid设置值对应
        String t2="<img src='cid:Doraemon' width='200px'><br>";
        //设置发送的文本，提醒用户查看附件
        String t3="<h3>您有附件未收取，请查看！</h3></body></html>";
        //拼接显示的文本
        helper.setText(t1+t2+t3,true);
        //添加图片资源
        helper.addInline("Doraemon", new File("D:\\Doraemon.jpg"));
        //添加附件
        helper.addAttachment("Doraemon1", new File("D:\\Doraemon.jpg"));
        //发送邮件
        mailSender.send(mimeMessage);
    }

    //发送模板邮件
    @Autowired
    private TemplateEngine templateEngine;
    @Test
    void sendTemplate() throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom("114056405@qq.com");
        helper.setTo("lvfeng@ccit.js.cn");
        helper.setSubject("激活邮件");
        // 利用 Thymeleaf 模板构建 html 文本
        Context ctx = new Context();
        // 设置 Thymeleaf 模板所需的变量值
        ctx.setVariable("username", "测试用户");
        // 执行 Thymeleaf 模板引擎，创建 html 文本
        // 默认 Thymeleaf 的所有模板都放在 resources/templates 目录下，并且以 .html 扩展名结尾
        String emailText = templateEngine.process("sendMail", ctx);
        // 设置要发送的 html 文本，第二个参数代表是否为 html 文本
        helper.setText(emailText, true);
        mailSender.send(mimeMessage);
    }
    /*
    发送带HTML标签的邮件
     */
    @Test
    void sendHtml() throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        helper.setFrom("114056405@qq.com");
        helper.setTo("lvfeng@ccit.js.cn");
        helper.setSubject("主题：HTML邮件");
        helper.setText("<html><body><h1>测试邮件内容<h1/></body></html>", true);

        mailSender.send(mimeMessage);
    }

    /**
     * 发送带图片的邮件
     */
    @Test
    void sendInline() throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom("114056405@qq.com");
        helper.setTo("lvfeng@ccit.js.cn");
        helper.setSubject("主题：Inline邮件");
        helper.setText("<html><body><img src='cid:head' width='200px'></body></html>", true);
        helper.addInline("head", new File("D:\\证书.png"));

        mailSender.send(mimeMessage);
    }

    /**
     * 发送带附件的邮件
     */
    @Test
    void sendAttachment() throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom("114056405@qq.com");
        helper.setTo("lvfeng@ccit.js.cn");
        helper.setSubject("主题：Attachment邮件");
        helper.setText("<html><body><h1>您有附件未收取，请查看！</h1></body></html>", true);
        helper.addAttachment("head1", new File("D:\\证书.png"));
        helper.addAttachment("head2", new File("D:\\证书.png"));

        mailSender.send(mimeMessage);
    }



}
