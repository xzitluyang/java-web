package cn.js.ccit.controller;

import cn.js.ccit.service.AsyncTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@EnableAsync
@RestController
public class AsyncController {
    @Autowired
    private AsyncTask asyncTask;
    @RequestMapping("asyncTask")
    public String test(){
        asyncTask.test();
        return "AsyncTask任务";
    }
}
