package cn.js.ccit.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class AsyncTask {
    //表示方法是异步方法
    @Async
    public void test(){
        try {
            System.out.println("请求时间："+ LocalDateTime.now());
            //需要睡眠等待5秒后再执行
            Thread.sleep(5000);
            System.out.println("响应时间："+LocalDateTime.now());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
