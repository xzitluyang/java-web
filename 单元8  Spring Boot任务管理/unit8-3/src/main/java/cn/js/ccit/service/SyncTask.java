package cn.js.ccit.service;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * 业务逻辑处理
 */
@Service
public class SyncTask {
    public void test(){
        try {
            System.out.println("请求时间："+ LocalDateTime.now());
            //需要睡眠等待5秒后再执行
            Thread.sleep(5000);
            System.out.println("响应时间："+LocalDateTime.now());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
