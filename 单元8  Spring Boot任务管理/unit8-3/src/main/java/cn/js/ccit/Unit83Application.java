package cn.js.ccit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

//开启异步任务
@EnableAsync
//开启基于注解的定时任务
@EnableScheduling
@SpringBootApplication
public class Unit83Application {

    public static void main(String[] args) {
        SpringApplication.run(Unit83Application.class, args);
    }

}
