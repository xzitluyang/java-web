package cn.js.ccit.controller;

import cn.js.ccit.service.SyncTask;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SyncController {
    @Autowired
    private SyncTask syncTask;
    @RequestMapping("syncTask")
    public String test(){
        syncTask.test();
        return "SyncTask";
    }
}
