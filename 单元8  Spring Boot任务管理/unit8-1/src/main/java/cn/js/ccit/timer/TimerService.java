package cn.js.ccit.timer;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

//scheduler定时器执行任务的类
@Service
public class TimerService {
    //每隔5秒打印输出信息
    @Scheduled(cron = "0/5 * * * * 0-7")
    public void hello() {
        System.out.println("hello....." + LocalDateTime.now());
    }
}
