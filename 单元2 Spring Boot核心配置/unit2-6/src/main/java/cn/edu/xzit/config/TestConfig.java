package cn.edu.xzit.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
public class TestConfig implements MyConfig {
    @Override
    public void config() {
        System.out.println("测试环境");
    }
}
