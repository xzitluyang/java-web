package cn.edu.xzit.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("dev")
@Configuration
public class DevConfig implements MyConfig {
    @Override
    public void config() {
        System.out.println("开发环境");
    }
}
