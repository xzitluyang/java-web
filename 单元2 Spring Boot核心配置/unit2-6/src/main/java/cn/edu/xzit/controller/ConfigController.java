package cn.edu.xzit.controller;

import cn.edu.xzit.config.MyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigController {
    @Autowired
    private MyConfig myConfig;

    @GetMapping("/hello")
    public String hello(){

        myConfig.config();
        return "hello Spring Boot";
    }
}
