package cn.edu.xzit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Unit23Application {

    public static void main(String[] args) {
        SpringApplication.run(Unit23Application.class, args);
    }

}
