package cn.edu.xzit.configuration;

import cn.edu.xzit.vo.Role;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyConfiguration {
    @Bean
    public String msg(){
        return "这是配置类中的返回信息";
    }
    @Bean
    public Role myRole(){
        return new Role();
    }
}

