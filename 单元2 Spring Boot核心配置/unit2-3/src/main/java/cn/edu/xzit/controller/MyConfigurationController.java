package cn.edu.xzit.controller;

import cn.edu.xzit.vo.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyConfigurationController {
    @Autowired
    private String msg;
    @Autowired
    private Role role;

    @GetMapping("/test")
    public String test() {
        return msg;
    }

    @GetMapping("/role")
    public String getRole() {
        role.setName("新的角色");
        return role.toString();
    }

}
