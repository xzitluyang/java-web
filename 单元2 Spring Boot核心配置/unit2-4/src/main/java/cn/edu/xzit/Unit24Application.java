package cn.edu.xzit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Unit24Application {

    public static void main(String[] args) {
        SpringApplication.run(Unit24Application.class, args);
    }

}
