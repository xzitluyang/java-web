package cn.edu.xzit.vo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

//角色实体
@Component

public class Role {
    private String name;
    private String description;
    private List<Long> permissionIds;
    @Override
    public String toString() {
        return "Role{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", permissionIds=" + permissionIds +
                '}';
    }
}

