package cn.edu.xzit.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.List;

@Configuration   //本类为配置类
@PropertySource("classpath:mytest.properties")     //指定自定义文件的位置和名称
@EnableConfigurationProperties(MyTestProperties.class)    //开启对应配置类的属性注入功能
@ConfigurationProperties(prefix = "role")   //指定配置文件注入属性的前缀

public class MyTestProperties {
    private String name;
    private String description;
    private List<Long> permissionIds;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Long> getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(List<Long> permissionIds) {
        this.permissionIds = permissionIds;
    }

    @Override
    public String toString() {
        return "MyTestProperties{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", permissionIds=" + permissionIds +
                '}';
    }
}


