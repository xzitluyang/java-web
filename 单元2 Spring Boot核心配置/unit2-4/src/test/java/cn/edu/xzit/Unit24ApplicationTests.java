package cn.edu.xzit;

import cn.edu.xzit.configuration.MyTestProperties;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Unit24ApplicationTests {

    @Autowired
    private MyTestProperties myTestProperties;
    @Test
    public void myTestProp(){
        System.out.println(myTestProperties);
    }

}
