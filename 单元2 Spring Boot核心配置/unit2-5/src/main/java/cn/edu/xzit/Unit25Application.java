package cn.edu.xzit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Unit25Application {

	public static void main(String[] args) {
		SpringApplication.run(Unit25Application.class, args);
	}

}
