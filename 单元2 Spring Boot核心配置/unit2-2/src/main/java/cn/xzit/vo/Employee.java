package cn.xzit.vo;


import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
@ConfigurationProperties(prefix="employee")
@Setter
@ToString
public class Employee {
    private String name;
    private Integer age;
    private List<String> hobby;
}
