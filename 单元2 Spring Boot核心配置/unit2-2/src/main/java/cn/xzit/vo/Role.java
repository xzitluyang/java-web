package cn.xzit.vo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

//角色实体
@Component
@ConfigurationProperties(prefix="role111")
public class Role {
//    @Value("${role.name}")
    private String name;
//    @Value("${role.description}")
    private String description;
    private List<Long> permissionIds;
    @Override
    public String toString() {
        return "Role{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", permissionIds=" + permissionIds +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPermissionIds(List<Long> permissionIds) {
        this.permissionIds = permissionIds;
    }
}

