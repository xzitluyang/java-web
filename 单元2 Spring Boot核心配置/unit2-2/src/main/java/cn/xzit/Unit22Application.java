package cn.xzit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Unit22Application {

    public static void main(String[] args) {
        SpringApplication.run(Unit22Application.class, args);
    }

}
