package com.controller;

import com.entity.User;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
    @Autowired
    private UserService userservice;

    @GetMapping("/user/{id}")
    public User getUser(@PathVariable("id") Integer id)
    {
        return userservice.getUserById(id);
    }
    @PostMapping("/user")
    public User update(User user)
    {
        return userservice.updateUser(user);
    }

    @GetMapping("/deluser")
    public String  deleteUser(int id)
    {
        userservice.userDelete(id);
        return "success";
    }
//    public User getUserBylastName(String lastName)
//    {
//        return userservice.getUserByLastName(lastName);
//    }
}
