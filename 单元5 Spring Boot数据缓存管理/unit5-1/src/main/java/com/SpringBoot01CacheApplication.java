package com;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/*一、搭建基本环境
//第一步：导入数据库，创建出两个表
//第二步：创建javabean封装数据
//第三步：整合Mybatis-plus，操作数据库
//      1、配置数据源信息
//      2、使用注解版的MyBatis-plus：
//          1）@MapperScan指定需要扫描的mapper接口所在的包
// 二、快速体验缓存
// 步骤：1、开启基于注解的给缓存@EnableCaching
//      2、标注缓存注解
    //      @Cacheable
    //      @CacheEvict

 */


@MapperScan("com.mapper")
@SpringBootApplication
//todo 1.开启基于注解的缓存
@EnableCaching//开启基于注解的缓存
@EnableTransactionManagement
public class SpringBoot01CacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBoot01CacheApplication.class, args);
    }

}


