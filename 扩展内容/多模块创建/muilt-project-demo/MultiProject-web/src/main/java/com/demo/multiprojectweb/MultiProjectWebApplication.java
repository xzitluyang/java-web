package com.demo.multiprojectweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiProjectWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultiProjectWebApplication.class, args);
    }

}
