drop table if exists user;

create table user
(
    id    bigint auto_increment primary key,
    name  VARCHAR(250) NOT NULL,
    age   int,
    email VARCHAR(250) NOT NULL
);


insert into user (name, age, email)
values ('John', 25, 'john@gmail.com'),
       ('Mike', 30, 'mike@gmail.com'),
       ('Mary', 28, 'mary@gmail.com');
