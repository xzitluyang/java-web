package cn.edu.xzit.Model;


import lombok.Data;

@Data
public class Customer {
    private Integer id;
    private String jobNo;
    private String name;
    private String department;

}
