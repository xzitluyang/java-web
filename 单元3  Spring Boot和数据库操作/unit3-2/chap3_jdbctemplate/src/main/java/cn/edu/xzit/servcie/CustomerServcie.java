package cn.edu.xzit.servcie;

import cn.edu.xzit.Model.Customer;

import java.util.List;

public interface CustomerServcie {
    //添加客户信息
    public int saveCustomer(Customer Customer);

    //获取所有客户信息
    public List<Customer> getAllCustomer();

    //根据ID查询客户信息
    public Customer getCustomerById(Integer id);

    //修改客户信息
    public int updateCustomer(Customer customer);

    //删除客户信息
    public int deleteCustomer(Integer id);
}
