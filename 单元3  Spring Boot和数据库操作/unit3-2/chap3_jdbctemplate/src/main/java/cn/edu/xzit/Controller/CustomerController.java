package cn.edu.xzit.Controller;

import cn.edu.xzit.servcie.CustomerServcie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {

    @Autowired
    CustomerServcie customerServcie;

    @PostMapping("/save")
    public String saveCustomer(@RequestBody Customer customer) {

        Customer cust = new Customer();
        cust.setName(customer.getName());
        cust.setJobNo(customer.getJobNo());
        cust.setDepartment(customer.getDepartment());

        int result = customerServcie.saveCustomer(customer);

        if (result > 0) {
            return "添加员工成功！";
        } else {
            return "添加员工失败！";
        }

    }

    @GetMapping("/getAll")
    public List<Customer> getAllCustomer() {
        return customerServcie.getAllCustomer();
    }

    //根据ID查询客户信息
    @GetMapping("/getCustomer/{id}")
    public Customer getCustomerById(@PathVariable Integer id) {
        return customerServcie.getCustomerById(id);
    }

    //修改客户信息
    @GetMapping("/update")
    public String updateCustomer(Customer customer) {
        customer.setId(9);
        customer.setName("孙贺祥");
        customer.setJobNo("J00110");
        customer.setDepartment("软件学院");
        int result = customerServcie.updateCustomer(customer);
        if (result > 0) {
            return "修改员工成功啦！";
        } else {
            return "修改员工失败啦！";
        }

    }

    //删除客户信息
    @GetMapping("/delete/{id}")
    public String deleteCustomer(@PathVariable Integer id) {
        int result = customerServcie.deleteCustomer(id);
        if (result > 0) {
            return "删除员工成功啦！";
        } else {
            return "删除员工失败！";
        }
    }
}
