package cn.edu.xzit.dao;

import cn.edu.xzit.Model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerDaoImpl implements CustomerDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    //添加客户信息
    @Override
    public int saveCustomer(Customer customer) {
        String sql = "insert into tb_customer(jobNo,name,departMent) values(?,?,?)";
        int result = jdbcTemplate.update(sql, new Object[]{customer.getJobNo(), customer.getName(), customer.getDepartment()});
        return result;
    }

    //获取所有客户信息
    @Override
    public List<Customer> getAllCustomer() {
        String sql = "select * from tb_customer";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Customer.class));
    }

    //根据id查询客户信息
    @Override
    public Customer getCustomerById(Integer id) {
        String sql = "select * from tb_customer where id=?";
        return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(Customer.class), id);
    }

    //修改客户信息
    @Override
    public int updateCustomer(Customer customer) {
        String sql = "update tb_customer set jobNo=?,name=?,departMent=? where id=?";
        return jdbcTemplate.update(sql, customer.getJobNo(), customer.getName(), customer.getDepartment(), customer.getId());
    }

    //删除客户信息
    @Override
    public int deleteCustomer(Integer id) {
        String sql = "delete from tb_customer where id=?";
        return jdbcTemplate.update(sql, id);
    }
}

