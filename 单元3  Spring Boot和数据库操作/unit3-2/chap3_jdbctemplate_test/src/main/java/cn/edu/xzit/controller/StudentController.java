package cn.edu.xzit.controller;

import cn.edu.xzit.entity.Student;
import cn.edu.xzit.service.StudentServcie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

    @Autowired
    StudentServcie studentServcie;

    @GetMapping("/save")
    public String saveStudent() {

        Student student = new Student();
        student.setStuNo("20082540101");
        student.setStuName("张一凡");
        student.setSex(1);
        student.setClassName("常大软件201");

        int result = studentServcie.saveStudent(student);

        if (result > 0) {
            return "添加学生成功！";
        } else {
            return "添加学生失败！";
        }

    }

    @GetMapping("/getAll")
    public String getAllStudent() {
        return studentServcie.getAllStudent().toString();
    }

    //根据ID查询客户信息
    @GetMapping("/getStudent/{id}")
    public Student getStudentById(@PathVariable Integer id) {
        return studentServcie.getStudentById(id);
    }

    //修改客户信息
    @GetMapping("/update")
    public String updateStudent(Student student) {
        student.setId(1);

        student.setStuNo("20082540101");
        student.setStuName("张一凡");
        student.setSex(0);
        student.setClassName("常大软件202");


        int result = studentServcie.updateStudent(student);
        if (result > 0) {
            return "修改学生成功啦！";
        } else {
            return "修改学生失败啦！";
        }

    }

    //删除学生信息
    @GetMapping("/delete/{id}")
    public String deleteStudent(@PathVariable Integer id) {
        int result = studentServcie.deleteStudent(id);
        if (result > 0) {
            return "删除学生信息成功啦！";
        } else {
            return "删除学生信息失败！";
        }
    }
}
