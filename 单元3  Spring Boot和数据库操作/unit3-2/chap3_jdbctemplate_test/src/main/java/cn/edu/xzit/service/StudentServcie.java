package cn.edu.xzit.service;

import cn.edu.xzit.entity.Student;

import java.util.List;

public interface StudentServcie {
    //添加学生信息
    public int saveStudent(Student student);

    //获取所有学生信息
    public List<Student> getAllStudent();

    //根据ID查询学生信息
    public Student getStudentById(Integer id);

    //修改学生信息
    public int updateStudent(Student student);

    //删除学生信息
    public int deleteStudent(Integer id);
}
