package cn.edu.xzit.entity;

import lombok.Data;

@Data
public class Student {
    private int id;
    private String stuNo;
    private String stuName;
    private int sex;
    private String className;

}
