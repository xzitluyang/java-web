package cn.edu.xzit.dao;

import cn.edu.xzit.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StudentDaoImpl implements StudentDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int saveStudent(Student student) {
        String sql = "insert into tb_student(stuNo,stuName,sex,className) values(?,?,?,?)";
        int result = jdbcTemplate.update(sql, new Object[]{student.getStuNo(), student.getStuName(), student.getSex(),student.getClassName()});
        return result;
    }

    @Override
    public List<Student> getAllStudent() {
        String sql = "select * from tb_student";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Student.class));
    }

    @Override
    public Student getStudentById(Integer id) {
        String sql = "select * from tb_student where id=?";
        return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(Student.class), id);
    }

    @Override
    public int updateStudent(Student student) {
        String sql = "update tb_student set stuNo=?,stuName=?,sex=?,className=? where id=?";
        return jdbcTemplate.update(sql, student.getStuNo(), student.getStuName(), student.getSex(), student.getClassName(), student.getId());
    }

    @Override
    public int deleteStudent(Integer id) {
        String sql = "delete from tb_student where id=?";
        return jdbcTemplate.update(sql, id);
    }
}

