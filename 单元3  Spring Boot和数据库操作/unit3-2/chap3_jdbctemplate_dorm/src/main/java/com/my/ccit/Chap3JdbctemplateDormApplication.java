package com.my.ccit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Chap3JdbctemplateDormApplication {

    public static void main(String[] args) {
        SpringApplication.run(Chap3JdbctemplateDormApplication .class, args);
    }

}
