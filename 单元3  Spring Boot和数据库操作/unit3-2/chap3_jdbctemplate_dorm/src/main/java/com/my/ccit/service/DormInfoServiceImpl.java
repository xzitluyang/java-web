package com.my.ccit.service;

import com.my.ccit.dao.DormInfoDao;
import com.my.ccit.entity.DormInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DormInfoServiceImpl implements DormInfoServcie {

    @Autowired
    DormInfoDao dormInfoDao;

    @Override
    public int saveDormInfo(DormInfo dormInfo) {
        return dormInfoDao.saveDormInfo(dormInfo);
    }

    @Override
    public List<DormInfo> getAllDormInfo() {
        return dormInfoDao.getAllDormInfo();
    }

    @Override
    public DormInfo getDormInfoById(Integer id) {
        return dormInfoDao.getDormInfoById(id);
    }

    @Override
    public int updateDormInfo(DormInfo dormInfo) {
        return dormInfoDao.updateDormInfo(dormInfo);
    }

    @Override
    public int deleteDormInfo(Integer id) {
        return dormInfoDao.deleteDormInfo(id);
    }
}
