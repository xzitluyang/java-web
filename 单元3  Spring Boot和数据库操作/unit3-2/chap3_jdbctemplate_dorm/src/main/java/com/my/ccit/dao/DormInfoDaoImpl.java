package com.my.ccit.dao;

import com.my.ccit.entity.DormInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DormInfoDaoImpl implements DormInfoDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int saveDormInfo(DormInfo dormInfo) {
        String sql = "insert into tb_dormInfo(dormNo,dormName,dormLocation,dormPhone,remarks) values(?,?,?,?,?)";
        int result = jdbcTemplate.update(sql, new Object[]{dormInfo.getDormNo(),dormInfo.getDormName(),
                                                            dormInfo.getDormLocation(),dormInfo.getDormPhone(),dormInfo.getRemarks()});
        return result;
    }

    @Override
    public List<DormInfo> getAllDormInfo() {
        String sql = "select * from tb_dormInfo";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(DormInfo.class));
    }

    @Override
    public DormInfo getDormInfoById(Integer id) {
        String sql = "select * from tb_dormInfo where id=?";
        return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(DormInfo.class), id);
    }

    @Override
    public int updateDormInfo(DormInfo dormInfo) {
        String sql = "update tb_dormInfo set dormNo=?,dormName=?,dormLocation=?,dormPhone=?,remarks=? where id=?";
        return jdbcTemplate.update(sql, dormInfo.getDormNo(),dormInfo.getDormName(),
                dormInfo.getDormLocation(),dormInfo.getDormPhone(),dormInfo.getRemarks(), dormInfo.getId());
    }

    @Override
    public int deleteDormInfo(Integer id) {
        String sql = "delete from tb_dormInfo where id=?";
        return jdbcTemplate.update(sql, id);
    }
}

