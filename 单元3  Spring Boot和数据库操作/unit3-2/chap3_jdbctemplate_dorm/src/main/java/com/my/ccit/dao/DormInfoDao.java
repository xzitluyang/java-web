package com.my.ccit.dao;

import com.my.ccit.entity.DormInfo;

import java.util.List;

public interface DormInfoDao {

    //添加宿舍信息
    public int saveDormInfo(DormInfo dormInfo);

    //获取所有宿舍信息
    public List<DormInfo> getAllDormInfo();

    //根据ID查询宿舍信息
    public DormInfo getDormInfoById(Integer id);

    //修改宿舍信息
    public int updateDormInfo(DormInfo dormInfo);

    //删除宿舍信息
    public int deleteDormInfo(Integer id);
}
