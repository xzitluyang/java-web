package com.my.ccit.controller;

import com.my.ccit.entity.DormInfo;
import com.my.ccit.service.DormInfoServcie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DormInfoController {

    @Autowired
    DormInfoServcie dormInfoServcie;

    @GetMapping("/save")
    public String saveDormInfo() {

        DormInfo dormInfo = new DormInfo();
        dormInfo.setDormNo("dorm01");
        dormInfo.setDormName("男生01宿舍");
        dormInfo.setDormLocation("南门西边第1栋楼");
        dormInfo.setDormPhone("83338180");
        dormInfo.setRemarks("宿舍共有1200张床位，300个房间，四人间");

        int result = dormInfoServcie.saveDormInfo(dormInfo);

        if (result > 0) {
            return "添加宿舍成功！";
        } else {
            return "添加宿舍失败！";
        }

    }

    @GetMapping("/getAll")
    public String getAllDormInfo() {
        return dormInfoServcie.getAllDormInfo().toString();
    }

    //根据ID查询客户信息
    @GetMapping("/getDormInfo/{id}")
    public DormInfo getDormInfoById(@PathVariable Integer id) {
        return dormInfoServcie.getDormInfoById(id);
    }

    //修改客户信息
    @GetMapping("/update")
    public String updateDormInfo(DormInfo dormInfo) {
        dormInfo.setId(1);
        dormInfo.setDormNo("dorm02");
        dormInfo.setDormName("女生01宿舍");
        dormInfo.setDormLocation("东门西边第1栋楼");
        dormInfo.setDormPhone("83338180");
        dormInfo.setRemarks("宿舍共有1000张床位，250个房间，四人间");


        int result = dormInfoServcie.updateDormInfo(dormInfo);
        if (result > 0) {
            return "修改宿舍成功啦！";
        } else {
            return "修改宿舍失败啦！";
        }

    }

    //删除宿舍信息
    @GetMapping("/delete/{id}")
    public String deleteDormInfo(@PathVariable Integer id) {
        int result = dormInfoServcie.deleteDormInfo(id);
        if (result > 0) {
            return "删除宿舍信息成功啦！";
        } else {
            return "删除宿舍信息失败！";
        }
    }
}
