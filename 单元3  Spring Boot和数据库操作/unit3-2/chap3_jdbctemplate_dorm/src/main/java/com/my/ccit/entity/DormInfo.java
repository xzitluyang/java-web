package com.my.ccit.entity;

import lombok.Data;

@Data
public class DormInfo {
    private int id;
    private String dormNo;
    private String dormName;
    private String dormLocation;
    private String dormPhone;
    private String remarks;

}
