package cn.edu.xzit.Dao;

import cn.edu.xzit.entity.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CourseDao extends JpaRepository<Course, Integer> {
//添加课程学习、修改课程信息、删除课程信息、课程信息显示、根据名称查询课程信息。

    @Override
    Page<Course> findAll(Pageable pageable);//分页查询

    @Query(value = "select * from tb_course  where courseNo=?1", nativeQuery = true)
    List<Course> findCourseByCourseNo(String courseNo);
}
