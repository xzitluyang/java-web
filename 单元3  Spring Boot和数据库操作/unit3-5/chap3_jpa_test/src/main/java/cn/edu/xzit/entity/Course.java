package cn.edu.xzit.entity;

import lombok.Data;

import javax.persistence.*;

@Entity(name = "tb_course")
@Data
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "courseNo", nullable = false)
    private String courseNo;

    @Column(name = "courseName", nullable = false)
    private String courseName;

    @Column(name = "teacherNo", nullable = false)
    private String teacherNo;

    @Column(name = "teacherName", nullable = false)
    private String teacherName;

    @Column(name = "credit", nullable = false)
    private String credit;

    @Column(name = "classHour", nullable = false)
    private String classHour;

    @Column(name = "coursePlan", nullable = false)
    private String coursePlan;


}
