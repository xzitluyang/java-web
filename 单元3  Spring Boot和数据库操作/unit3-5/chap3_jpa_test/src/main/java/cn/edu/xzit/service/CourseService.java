package cn.edu.xzit.service;

import cn.edu.xzit.Dao.CourseDao;
import cn.edu.xzit.entity.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {
    final
    CourseDao courseDao;

    public CourseService(CourseDao courseDao) {
        this.courseDao = courseDao;
    }

    public List<Course> findAllCourse() {
        return courseDao.findAll();
    }

    public Course saveCourse(Course course) {
        return courseDao.save(course);
    }

    public Course updateCourse(Course course) {
        return courseDao.save(course);
    }

    public List<Course> findCourseByCourseNo(String courseNo) {
        return courseDao.findCourseByCourseNo(courseNo);
    }

    public Course getById(Integer id) {
        return courseDao.getById(id);
    }


    public void deleteById(Integer id) {
        courseDao.deleteById(id);
    }


    public Page<Course> findAll(Pageable pageable) {
        return courseDao.findAll(pageable);
    }


}
