package cn.edu.xzit.Dao;

import cn.edu.xzit.entity.AddressBooks;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressBooksDao extends JpaRepository<AddressBooks, Integer> {

    @Query(value="select * from tb_addressbooks where contactsName=?1",nativeQuery = true)
    public List<AddressBooks> findAddressBooksByContactsName(String contactsName);

}
