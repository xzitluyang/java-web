package cn.edu.xzit.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@Entity(name = "tb_addressbooks")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer"})
public class AddressBooks implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String contactsName;
    private String phone;
    private String classification;
    private String weChat;
    private String qq;
    private String email;
    private String remarks;
    private String address1111111111;

}
