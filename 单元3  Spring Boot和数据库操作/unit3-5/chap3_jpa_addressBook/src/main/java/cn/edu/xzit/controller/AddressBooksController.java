package cn.edu.xzit.controller;

import cn.edu.xzit.entity.AddressBooks;
import cn.edu.xzit.service.AddressBooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AddressBooksController<addressBooksService> {

    @Autowired
    AddressBooksService addressBooksService;

    @GetMapping("/findAllAddressBooks")
    public String findAllAddressBooks() {
        List<AddressBooks> addressBooks = addressBooksService.findAllAddressBooks();
        return addressBooks.toString();
    }

    @GetMapping("/saveAddressBooks")
    public AddressBooks saveAddressBooks() {
       AddressBooks addressBooks=new AddressBooks();
       addressBooks.setContactsName("张三");
       addressBooks.setPhone("13656****");
       addressBooks.setWeChat("zhangsan");
       addressBooks.setQq("5678****");
       addressBooks.setClassification("同事");
       addressBooks.setEmail("zhangsan***@qq.com");
       addressBooks.setRemarks("教研室");
       addressBooksService.saveAddressBooks(addressBooks);
       return addressBooks;
    }

    @GetMapping("/getAddressBooksById/{id}")
    public AddressBooks getAddressBooksById(@PathVariable Integer id) {
        AddressBooks addressBooks = addressBooksService.getByAddressBooksId(id);
        return addressBooks;
    }

    @GetMapping("/findCourseByContactsName/{contactsName}")
    public List<AddressBooks> findCourseByContactsName(@PathVariable  String contactsName) {
        return addressBooksService.findAddressBooksByContactsName(contactsName);
    }

    @GetMapping("/deleteAddressBooksById/{id}")
    public String deleteAddressBooksById(@PathVariable Integer id) {
        addressBooksService.deleteAddressBooksById(id);
        return "删除联系人成功啦！";
    }

    @GetMapping("/updateAddressBooks")
    public String  updateAddressBooks(){
        AddressBooks addressBooks=new AddressBooks();
        addressBooks.setId(1);
        addressBooks.setContactsName("张三丰");
        addressBooks.setPhone("13356****");
        addressBooks.setWeChat("zhangsanfen");
        addressBooks.setQq("5678****");
        addressBooks.setClassification("同学");
        addressBooks.setEmail("zhangsan***@qq.com");
        addressBooks.setRemarks("教研室");
        addressBooksService.updateAddressBooks(addressBooks);
        return "修改联系人成功啦！";
    }


}
