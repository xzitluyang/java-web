package cn.edu.xzit.service;

import cn.edu.xzit.Dao.AddressBooksDao;
import cn.edu.xzit.entity.AddressBooks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AddressBooksService {

    //添加联系人、修改联系人、删除联系人、查询联系人。
    @Autowired
    AddressBooksDao addressBooksDao;

    public List<AddressBooks> findAllAddressBooks() {
        return addressBooksDao.findAll();
    }

    @Transactional
    public AddressBooks saveAddressBooks(AddressBooks addressBooks) {
        int[] x = {1, 2, 3, 4, 5};
        System.out.println("x[5]=" + x[5]);
        return addressBooksDao.save(addressBooks);
    }

    public AddressBooks updateAddressBooks(AddressBooks addressBooks) {
        return addressBooksDao.save(addressBooks);
    }

    public List<AddressBooks> findAddressBooksByContactsName(String contactsName) {
        return addressBooksDao.findAddressBooksByContactsName(contactsName);
    }

    public AddressBooks getByAddressBooksId(Integer id) {
        return addressBooksDao.getById(id);
    }


    public void deleteAddressBooksById(Integer id) {
        addressBooksDao.deleteById(id);
    }


}
