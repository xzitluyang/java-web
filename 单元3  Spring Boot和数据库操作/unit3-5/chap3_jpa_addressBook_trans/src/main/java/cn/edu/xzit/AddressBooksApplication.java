package cn.edu.xzit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AddressBooksApplication {

    public static void main(String[] args) {
        SpringApplication.run(AddressBooksApplication.class, args);
    }

}
