package cn.edu.xzit.Dao;

import cn.edu.xzit.entity.AddressBooks;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AddressBooksDao extends JpaRepository<AddressBooks, Integer> {

    @Query(value = "select * from tb_addressbooks where contactsName=?1", nativeQuery = true)
    List<AddressBooks> findAddressBooksByContactsName(String contactsName);

}
