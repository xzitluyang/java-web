package cn.edu.xzit.customerjpademo.controller;

import cn.edu.xzit.customerjpademo.service.CustomerService;
import cn.edu.xzit.customerjpademo.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping("/findAllCustomer")
    public List<Customer> findAllCustomer() {
        List<Customer> customers = customerService.findAllCustomer();
//        System.out.println(customers.size());
//        System.out.println(customers);
        return customers;

    }

    @GetMapping("/save")
    public Customer saveCustomer() {
        Customer customer = new Customer();
        customer.setJobNo("J00107");
        customer.setDepartment("网络安全");
        customer.setName("於奉城");
        System.out.println("--------------------");
       return customerService.saveCustomer(customer);

    }


    @GetMapping("/getCustomer/{id}")
    public Customer getCustomerById(@PathVariable Integer id) {
        Customer customer = customerService.getCustomerById(id);
        System.out.println(customer);
        return customer;
    }

    @GetMapping("/deleteCustomer/{id}")
    public void deleteCustomerById(@PathVariable Integer id) {
        customerService.deleteCustomerById(id);
        System.out.println("删除成功啦！");
    }


    @GetMapping("/findAll")
    public Page<Customer> findAll() {
        PageRequest pageable = PageRequest.of(0, 2);
        Page<Customer> page = customerService.findAll(pageable);
        System.out.println("总页数：" + page.getTotalPages());
        System.out.println("总的记录数：" + page.getTotalElements());
        System.out.println("查询结果：" + page.getContent());
        System.out.println("当前页数：" + page.getNumber() + 1);
        System.out.println("当前记录数：" + page.getNumberOfElements());
        System.out.println("每一页记录数：" + page.getSize());
        page.toList().forEach(System.out::println);
        return page;
    }

    @GetMapping("/findByName")
    public List<Customer> getByName(@RequestParam String name) {
        List<Customer> customers = customerService.getByName(name);
        System.out.println(customers);
        return customers;
    }


}
