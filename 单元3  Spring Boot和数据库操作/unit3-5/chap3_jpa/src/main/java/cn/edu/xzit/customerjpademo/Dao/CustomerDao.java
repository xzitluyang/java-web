package cn.edu.xzit.customerjpademo.Dao;

import cn.edu.xzit.customerjpademo.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface CustomerDao extends JpaRepository<Customer, Integer> {
    public List<Customer> findByName(String name);
}
