package cn.edu.xzit.customerjpademo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerjpademoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CustomerjpademoApplication.class, args);
    }

}
