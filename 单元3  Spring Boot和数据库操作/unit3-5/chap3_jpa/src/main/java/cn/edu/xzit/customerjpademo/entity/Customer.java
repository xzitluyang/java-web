package cn.edu.xzit.customerjpademo.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * 顾客
 *
 * @author 陆杨
 * @date 2024/04/11
 */
@Entity(name = "tb_customer")
@Data
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "jobNo", nullable = false)
    private String jobNo;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "department", nullable = false)
    private String department;

}
