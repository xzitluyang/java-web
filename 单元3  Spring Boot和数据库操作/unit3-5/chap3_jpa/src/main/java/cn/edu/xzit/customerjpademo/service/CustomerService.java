package cn.edu.xzit.customerjpademo.service;

import cn.edu.xzit.customerjpademo.Dao.CustomerDao;
import cn.edu.xzit.customerjpademo.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {
    @Autowired
    CustomerDao customerDao;

    public List<Customer> findAllCustomer() {
        return customerDao.findAll();
    }

    public Customer saveCustomer(Customer customer) {
        return customerDao.save(customer);
    }

    public Customer getCustomerById(Integer id) {
        return customerDao.getById(id);
    }

    public List<Customer> getByName(String name) {
        return customerDao.findByName(name);
    }

    public void deleteCustomerById(Integer id) {
        customerDao.deleteById(id);
    }

    public Page<Customer> findAll(Pageable pageable) {
        return customerDao.findAll(pageable);
    }



}
