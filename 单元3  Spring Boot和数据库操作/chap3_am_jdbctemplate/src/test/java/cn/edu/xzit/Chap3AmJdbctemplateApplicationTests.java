package cn.edu.xzit;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootTest
class Chap3AmJdbctemplateApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void createtime(){
        System.out.printf( new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
    }

}
