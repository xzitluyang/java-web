package cn.edu.xzit.controller;

import cn.edu.xzit.entity.Sys_role;
import cn.edu.xzit.service.Sys_roleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


@RestController
public class Sys_Controller {

    @Autowired
    Sys_roleService sys_roleService;

    @PostMapping("/saveRole")
    public String saveSys_role(@RequestBody Sys_role sys_role){

        Sys_role role = new Sys_role();
        role.setName(sys_role.getName());
        role.setDescription(sys_role.getDescription());
        role.setCreate_time(new java.sql.Date(new Date().getTime()));
        role.setCreate_by(null);
        role.setUpdate_time(new java.sql.Date(new Date().getTime()));
        role.setUpdate_by(null);
        role.setDel_flag(0);
        int result = sys_roleService.saveSys_role(sys_role);

        if (result > 0) {
            return "添加角色成功！";
        } else {
            return "添加角色失败！";
        }

    }

    @GetMapping("/getAllRole")
    public List<Sys_role> getAllSys_Role() {
        return sys_roleService.getAllSys_role();
    }

    //根据ID查询角色信息
    @GetMapping("/getSysRoleById/{id}")
    public Sys_role getSysRoleById(@PathVariable Integer id) {
        return sys_roleService.getSys_roleById(id);
    }

    //修改角色信息
    @GetMapping("/updateSysRole")
    public String updateSysRole(Sys_role sys_role) {
        sys_role.setId(2);
        sys_role.setName("总经理助理1");
        sys_role.setDescription("辅助总经理工作1");
        sys_role.setCreate_time(new java.sql.Date(new Date().getTime()));
        sys_role.setCreate_by(null);
        sys_role.setUpdate_time(new java.sql.Date(new Date().getTime()));
        sys_role.setUpdate_by(null);
        sys_role.setDel_flag(0);
        int result = sys_roleService.updateSys_role(sys_role);
        if (result > 0) {
            return "修改员工成功啦！";
        } else {
            return "修改员工失败啦！";
        }

    }

    //删除角色信息
    @GetMapping("/deleteSysRoleById/{id}")
    public String deleteSysRoleById(@PathVariable Integer id) {
        int result = sys_roleService.deleteSys_roleById(id);
        if (result > 0) {
            return "删除角色成功啦！";
        } else {
            return "删除角色失败！";
        }
    }

}
