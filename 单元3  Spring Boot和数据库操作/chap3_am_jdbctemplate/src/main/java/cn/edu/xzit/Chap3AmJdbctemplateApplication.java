package cn.edu.xzit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Chap3AmJdbctemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(Chap3AmJdbctemplateApplication.class, args);
    }

}
