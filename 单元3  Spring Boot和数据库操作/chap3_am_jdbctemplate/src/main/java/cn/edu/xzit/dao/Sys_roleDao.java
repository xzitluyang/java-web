package cn.edu.xzit.dao;

import cn.edu.xzit.entity.Sys_role;

import java.util.List;

public interface Sys_roleDao {

    //添加角色信息
    public int saveSys_role(Sys_role sys_role);

    //获取所有角色信息
    public List<Sys_role> getAllSys_role();

    //根据ID查询角色信息
    public Sys_role getSys_roleById(Integer id);

    //修改角色信息
    public int updateSys_role(Sys_role sys_role);

    //根据ID删除角色信息
    public int deleteSys_roleById(Integer id);
}
