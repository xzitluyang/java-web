package cn.edu.xzit.service;

import cn.edu.xzit.dao.Sys_roleDao;
import cn.edu.xzit.entity.Sys_role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Sys_roleServiceImpl implements Sys_roleService {

    @Autowired
    private Sys_roleDao sys_roleDao;


    @Override
    public int saveSys_role(Sys_role sys_role) {
        return sys_roleDao.saveSys_role(sys_role);
    }

    @Override
    public List<Sys_role> getAllSys_role() {
        return sys_roleDao.getAllSys_role();
    }

    @Override
    public Sys_role getSys_roleById(Integer id) {
        return sys_roleDao.getSys_roleById(id);
    }

    @Override
    public int updateSys_role(Sys_role sys_role) {
        return sys_roleDao.updateSys_role(sys_role);
    }

    @Override
    public int deleteSys_roleById(Integer id) {
        return sys_roleDao.deleteSys_roleById(id);
    }
}
