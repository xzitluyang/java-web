package cn.edu.xzit.dao;

import cn.edu.xzit.entity.Sys_role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class Sys_roleDaoImpl implements Sys_roleDao {


    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public int saveSys_role(Sys_role sys_role) {
        String sql = "insert into sys_role(name,description,create_time,create_by,update_time,update_by,del_flag) values(?,?,?,?,?,?,?)";
        int result = jdbcTemplate.update(sql, new Object[]{
                sys_role.getName(), sys_role.getDescription(), sys_role.getCreate_time(),
                sys_role.getCreate_by(), sys_role.getUpdate_time(), sys_role.getUpdate_by(), sys_role.getDel_flag()});
        return result;
    }

    @Override
    public List<Sys_role> getAllSys_role() {
        String sql = "select * from sys_role";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Sys_role.class));
    }

    @Override
    public Sys_role getSys_roleById(Integer id) {
        String sql = "select * from sys_role where id=?";
        return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(Sys_role.class), id);
    }

    @Override
    public int updateSys_role(Sys_role sys_role) {
        String sql = "update sys_role set name=?,description=?,create_time=?,create_by=?,update_time=?,del_flag=? where id=?";
        return jdbcTemplate.update(sql, sys_role.getName(), sys_role.getDescription(), sys_role.getCreate_time(),
                sys_role.getCreate_by(), sys_role.getUpdate_time(), sys_role.getDel_flag(), sys_role.getId());
    }

    @Override
    public int deleteSys_roleById(Integer id) {
        String sql = "delete from sys_role where id=?";
        return jdbcTemplate.update(sql, id);
    }

}
