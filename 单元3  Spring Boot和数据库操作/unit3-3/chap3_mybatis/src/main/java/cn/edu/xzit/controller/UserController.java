package cn.edu.xzit.controller;

import cn.edu.xzit.entity.User;
import cn.edu.xzit.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/getUserById/{id}")
    public String getUserById(@PathVariable Integer id) {
        System.out.println("id=" + id);
        return userService.getUserById(id).toString();
    }

    @PostMapping("/addUser")
    public String addUser(User user) {
        System.out.println("user=" + user);
        return userService.adduser(user) ? "success" : "fail";
    }

}
