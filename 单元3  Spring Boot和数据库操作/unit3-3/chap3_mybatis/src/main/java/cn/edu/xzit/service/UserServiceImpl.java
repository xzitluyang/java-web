package cn.edu.xzit.service;

import cn.edu.xzit.entity.User;
import cn.edu.xzit.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User getUserById(Integer id) {
        return userMapper.getUserById(id);
    }

    @Override
    public boolean adduser(User user) {
        return userMapper.adduser(user);
    }
}
