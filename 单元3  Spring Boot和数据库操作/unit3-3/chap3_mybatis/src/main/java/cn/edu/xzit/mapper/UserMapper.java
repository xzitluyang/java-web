package cn.edu.xzit.mapper;

import cn.edu.xzit.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {

    User getUserById(Integer id);

     boolean adduser(User user);
}
