package cn.edu.xzit;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

@MapperScan("com.my.ccit.mapper")
public class Chap3MybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(Chap3MybatisApplication.class, args);
    }

}
