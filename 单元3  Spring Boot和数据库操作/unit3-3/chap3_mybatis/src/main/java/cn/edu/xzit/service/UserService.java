package cn.edu.xzit.service;

import cn.edu.xzit.entity.User;

public interface UserService {
    User getUserById(Integer id);

    boolean adduser(User user);
}
