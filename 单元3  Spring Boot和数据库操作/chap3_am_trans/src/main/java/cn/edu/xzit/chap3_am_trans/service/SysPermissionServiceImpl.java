package cn.edu.xzit.chap3_am_trans.service;

import cn.edu.xzit.chap3_am_trans.dao.SysPermissionDao;
import cn.edu.xzit.chap3_am_trans.entity.SysPermission;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Service("sysPermissionService")
public class SysPermissionServiceImpl implements SysPermissionService {

    @Resource
    private SysPermissionDao sysPermissionDao;


    @Transactional
    public void insertSysPermission(SysPermission sysPermission) {
        sysPermissionDao.save(sysPermission);
        int[] array = {1, 2, 3};
        System.out.println(array[3]);
    }


}

