package cn.edu.xzit.chap3_am_trans.controller;

import cn.edu.xzit.chap3_am_trans.service.SysPermissionService;
import cn.edu.xzit.chap3_am_trans.entity.SysPermission;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@RestController
@RequestMapping("/sysPermission")
public class SysPermissionController {

    @Resource
    private SysPermissionService sysPermissionService;

    @RequestMapping("/save")
    public String saveSysPermission() {
        try {
            SysPermission sysPermission = new SysPermission();
            sysPermission.setPid(85l);
            sysPermission.setName("撤销");
            sysPermission.setPath(null);
            sysPermission.setIcon(null);
            sysPermission.setSort(3);
            sysPermission.setKeyword(null);
            sysPermission.setPermission(null);
            sysPermission.setType(2);
            sysPermission.setStatus(0);
            sysPermission.setCreateTime(null);
            sysPermission.setCreateBy(null);
            sysPermission.setDelFlag(0);

            sysPermissionService.insertSysPermission(sysPermission);

            return "添加权限成功啦！";
        } catch (Exception e) {
            System.out.println(e.toString());
            return "添加权限失败啦！";
        }
    }
}
