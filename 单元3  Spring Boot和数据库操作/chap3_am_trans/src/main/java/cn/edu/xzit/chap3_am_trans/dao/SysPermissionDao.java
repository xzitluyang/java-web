package cn.edu.xzit.chap3_am_trans.dao;

import cn.edu.xzit.chap3_am_trans.entity.SysPermission;
import org.springframework.data.jpa.repository.JpaRepository;


public interface SysPermissionDao extends JpaRepository<SysPermission, Integer> {
}
