package cn.edu.xzit.chap3_am_trans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Chap3AmTransApplication {

    public static void main(String[] args) {
        SpringApplication.run(Chap3AmTransApplication.class, args);
    }

}
