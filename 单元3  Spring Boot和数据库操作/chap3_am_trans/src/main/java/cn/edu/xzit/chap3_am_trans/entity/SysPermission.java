package cn.edu.xzit.chap3_am_trans.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "sys_permission")
public class SysPermission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long pid;
    private String name;
    private String path;
    private String icon;
    private Integer sort;
    private String permission;
    private Integer type;
    private Integer status;
    private String keyword;
    private Long createTime;
    private Long createBy;
    private Integer delFlag;

}
