package cn.edu.xzit;

import cn.edu.xzit.entity.Sys_Department;
import cn.edu.xzit.mapper.Sys_DepartmentMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
class TestMybatisPlusApplicationTests {

    @Resource
    private Sys_DepartmentMapper sys_departmentMapper;

    @Test
    public void queryUserForPage() {
        IPage<Sys_Department> userPage = new Page<>(1, 2);//参数一是当前页，参数二是每页个数
        userPage = sys_departmentMapper.selectPage(userPage, null);
        List<Sys_Department> list = userPage.getRecords();
        for (Sys_Department sys_department : list) {
            System.out.println(sys_department);
        }
    }
}


