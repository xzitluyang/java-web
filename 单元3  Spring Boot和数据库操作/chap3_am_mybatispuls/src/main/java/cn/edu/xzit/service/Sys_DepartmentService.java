package cn.edu.xzit.service;

import cn.edu.xzit.entity.Sys_Department;
import cn.edu.xzit.entity.Sys_DepartmentVo;

import java.util.List;

public interface Sys_DepartmentService {
    public List<Sys_Department> getAllSys_Departments();

    public Sys_Department getSys_DepartmentById(Integer id);

    public boolean insertSys_Department(Sys_Department sys_department);

    public boolean deleteSys_DepartmentById(Integer id);

    public boolean updateSys_Department(Sys_Department sys_department);

    public Sys_DepartmentVo queryList(Integer current, Integer size);
}
