package cn.edu.xzit.entity;

import lombok.Data;

import java.util.List;

@Data
public class Sys_DepartmentVo {
    private Integer current;//当前页
    private Integer size;//每一页数量
    private Long total;//总数
    private List<Sys_Department> sys_departmentList;//每一页的内容

    public Integer getCurrent() {
        return current;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<Sys_Department> getSys_departmentList() {
        return sys_departmentList;
    }

    public void setSys_departmentList(List<Sys_Department> sys_departmentList) {
        this.sys_departmentList = sys_departmentList;
    }

}
