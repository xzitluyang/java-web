package cn.edu.xzit.controller;

import cn.edu.xzit.entity.Sys_Department;
import cn.edu.xzit.entity.Sys_DepartmentVo;
import cn.edu.xzit.service.Sys_DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class Sys_DepartmentController {

    @Autowired
    private Sys_DepartmentService sysDepartmentService;

    @GetMapping("/getAllSys_Departments")
    public List<Sys_Department> getAllSysDepartments() {
        return sysDepartmentService.getAllSys_Departments();
    }


    @GetMapping("/getSys_DepartmentById/{id}")
    public Sys_Department getSys_DepartmentById(@PathVariable Integer id) {
        return sysDepartmentService.getSys_DepartmentById(id);
    }


    @GetMapping("/insertSys_Department")
    public String insertSys_Department() {
        Sys_Department sys_department = new Sys_Department();
        sys_department.setName("企划营销部-05");
        sys_department.setPid(15);
        sys_department.setDescription("企划营销部-05");
        sys_department.setCreate_time(new java.sql.Date(new Date().getTime()));
        sys_department.setCreate_by(Integer.toString(3));
        sys_department.setUpdate_time(new java.sql.Date(new Date().getTime()));
        sys_department.setUpdate_by(null);
        sys_department.setDel_flag(0);
        sys_department.setSub_ids(null);

        boolean result = sysDepartmentService.insertSys_Department(sys_department);
        if (result) {
            return "添加部门成功！";
        } else {
            return "添加部门失败！";
        }

    }

    @GetMapping("/deleteSys_DepartmentById/{id}")
    public String deleteSys_DepartmentById(@PathVariable Integer id) {
        boolean result = sysDepartmentService.deleteSys_DepartmentById(id);
        if (result) {
            return "删除部门成功！";
        } else {
            return "删除部门失败！";
        }
    }


    @GetMapping("/updateSys_Department")
    public String updateSys_Department() {


        Sys_Department sys_department = new Sys_Department();
        sys_department.setId(2);
        sys_department.setPid(15);
        sys_department.setDescription("企划销售部-031");
        sys_department.setCreate_time(new java.sql.Date(new Date().getTime()));
        sys_department.setCreate_by(Integer.toString(3));
        sys_department.setUpdate_time(new java.sql.Date(new Date().getTime()));
        sys_department.setUpdate_by(null);
        sys_department.setDel_flag(1);
        sys_department.setSub_ids(null);

        boolean result = sysDepartmentService.updateSys_Department(sys_department);
        if (result) {
            return "修改部门成功！";
        } else {
            return "修改部门失败！";
        }
    }

    @GetMapping("/querySys_Department/{current}/{size}")
    public Sys_DepartmentVo queryList(@PathVariable Integer current, @PathVariable Integer size) {
        return sysDepartmentService.queryList(current, size);
    }


}
