package cn.edu.xzit.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("sys_department")// 映射
public class Sys_Department {
    private int id;
    private int pid;//上级部门
    private String name;//部门名称
    private String description;//描述
    private Date create_time;//创建时间
    private String create_by;//创建人
    private Date update_time;//修改时间
    private String update_by;//修改人
    private int del_flag;//是否删除
    private String sub_ids;//所有子部门id
}
