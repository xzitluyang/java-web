package cn.edu.xzit.mapper;

import cn.edu.xzit.entity.Sys_Department;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface Sys_DepartmentMapper extends BaseMapper<Sys_Department> {
}


