package cn.edu.xzit;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.my.ccit.chap3.mapper")
public class Chap3AmMybatispulsApplication {

    public static void main(String[] args) {
        SpringApplication.run(Chap3AmMybatispulsApplication.class, args);
    }

}
