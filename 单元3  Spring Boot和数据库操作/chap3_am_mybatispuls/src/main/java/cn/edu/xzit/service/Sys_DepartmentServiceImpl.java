package cn.edu.xzit.service;

import cn.edu.xzit.entity.Sys_Department;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.edu.xzit.entity.Sys_DepartmentVo;
import cn.edu.xzit.mapper.Sys_DepartmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Sys_DepartmentServiceImpl implements Sys_DepartmentService {

    @Autowired
    private Sys_DepartmentMapper sys_departmentMapper;

    public List<Sys_Department> getAllSys_Departments() {
        List<Sys_Department> sys_departments = sys_departmentMapper.selectList(null);
        System.out.println(sys_departments);
        return sys_departments;
    }

    @Override
    public Sys_Department getSys_DepartmentById(Integer id) {
        return sys_departmentMapper.selectById(id);
    }

    @Override
    public boolean insertSys_Department(Sys_Department sys_department) {
        return sys_departmentMapper.insert(sys_department) > 0 ? true : false;
    }

    @Override
    public boolean deleteSys_DepartmentById(Integer id) {
        return sys_departmentMapper.deleteById(id) > 0 ? true : false;
    }

    @Override
    public boolean updateSys_Department(Sys_Department sys_department) {
        return sys_departmentMapper.updateById(sys_department) > 0 ? true : false;
    }

    @Override
    public Sys_DepartmentVo queryList(Integer current, Integer size) {
        Sys_DepartmentVo sys_departmentVo = new Sys_DepartmentVo();
        IPage<Sys_Department> page = new Page<>(current, size);
        sys_departmentMapper.selectPage(page, null);
        sys_departmentVo.setCurrent(current);
        sys_departmentVo.setSize(size);
        sys_departmentVo.setTotal(page.getTotal());
        sys_departmentVo.setSys_departmentList(page.getRecords());
        return sys_departmentVo;
    }


}
