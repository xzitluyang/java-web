package cn.edu.xzit.dao;

import cn.edu.xzit.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AccountDao extends JpaRepository<Account, Integer> {
}
