package cn.edu.xzit.service;

import cn.edu.xzit.dao.AccountDao;
import cn.edu.xzit.entity.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountDao accountDao;

    @Transactional
    public void transferAccounts(int fromUserId, int toUserId, float account) {

            Account fromUserAccount = accountDao.getById(fromUserId);
            fromUserAccount.setBalance(fromUserAccount.getBalance() - account);
            accountDao.save(fromUserAccount); // fromUser扣钱

            Account toUserAccount = accountDao.getById(toUserId);
            toUserAccount.setBalance(toUserAccount.getBalance() + account);
            //假设转账的时候假如出现异常，业务类或业务方法中没有使用@Transactional控制事务，则会出现钱转出了，收钱人没有收到的情况
            int zero = 1 / 0;  //这里我们先把这个异常注释掉，稍后我们再打开
            accountDao.save(toUserAccount); // toUser加钱

    }

    @Override
    public List<Account> getAll() {
        return accountDao.findAll();
    }


}
