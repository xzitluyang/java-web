package cn.edu.xzit.service;

import cn.edu.xzit.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountService  {
    /**
     *     * 转账
     *     * @param fromUserId
     *     * @param toUserId
     *     * @param account
     *    
     */
    void transferAccounts(int fromUserId, int toUserId, float account);

    /**
     *     * 查询所有账户
     *     * @return
     *    
     */
    List<Account> getAll();
}
