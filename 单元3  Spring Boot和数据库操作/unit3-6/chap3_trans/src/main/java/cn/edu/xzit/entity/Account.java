package cn.edu.xzit.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "tb_account")
@Data
public class Account {
    /**
     * * id 编号
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 用户名
     *  
     */
    @Column(length = 50)
    private String userName;

    /**
     * * 余额
     */
    private float balance;


}
