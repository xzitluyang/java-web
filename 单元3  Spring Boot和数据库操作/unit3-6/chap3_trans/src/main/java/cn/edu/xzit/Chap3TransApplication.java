package cn.edu.xzit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class Chap3TransApplication {

    public static void main(String[] args) {
        SpringApplication.run(Chap3TransApplication.class, args);
    }

}
