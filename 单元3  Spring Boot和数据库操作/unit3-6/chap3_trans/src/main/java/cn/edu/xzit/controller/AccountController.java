package cn.edu.xzit.controller;

import cn.edu.xzit.entity.Account;
import cn.edu.xzit.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping("/transfer")
    public String transferAccounts() {
        try {
            //1号张三  给2号李四  转账200元
            accountService.transferAccounts(1, 2, 200);
            return "转账成功！";
        } catch (Exception e) {
            System.out.println(e.toString());
            return "转账失败";
        }
    }

    @GetMapping("/getAll")
    public List<Account> getAll() {
        return accountService.getAll();
    }
}
