package com.my.ccit.service;

import com.my.ccit.dao.FreshmanDao;
import com.my.ccit.entity.Freshman;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

@Service("freshmanService")
public class FreshmanServiceImpl implements FreshmanService {

    @Resource
    private FreshmanDao freshmanDao;

    @Transactional
    public void saveFreshman(Freshman freshman) {
        freshmanDao.save(freshman);

        File f = new File(freshman.getFreshImage());
        try {
            FileReader fr = new FileReader(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
