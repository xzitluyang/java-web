package com.my.ccit.service;

import com.my.ccit.entity.Freshman;

public interface FreshmanService {

    //保存新生信息
    void saveFreshman(Freshman freshman);
}
