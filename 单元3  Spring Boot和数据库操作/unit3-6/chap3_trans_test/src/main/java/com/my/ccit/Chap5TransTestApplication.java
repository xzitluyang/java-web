package com.my.ccit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = "com")
@EnableTransactionManagement
public class Chap5TransTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(Chap5TransTestApplication.class, args);
    }

}
