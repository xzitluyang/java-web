package com.my.ccit.controller;

import com.my.ccit.entity.Freshman;
import com.my.ccit.service.FreshmanService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@RestController
@RequestMapping("/freshman")
public class FreshmanController {

    @Resource
    private FreshmanService freshmanService;

    @RequestMapping("/save")
    public String saveFreshman(Freshman freshman) {
        try {
            freshman.setFreshNo("001");
            freshman.setFreshName("张三");
            freshman.setFreshPhone("1335819****");
            freshman.setRemarks("软件技术专业");
            freshman.setFreshImage("c:\\temp.jpg");
            freshmanService.saveFreshman(freshman);
            return "添加员工成功！";
        } catch (Exception e) {
            System.out.println(e);
            return "添加员工失败";
        }
    }
}
