package com.my.ccit.entity;

import lombok.Data;

import javax.persistence.*;


@Entity
@Table(name = "tb_freshman")
@Data
public class Freshman {
    /**
     *   * id 编号
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "freshNo")
    private String freshNo;

    @Column(name = "freshName")
    private String freshName;

    @Column(name = "freshImage")
    private String freshImage;

    @Column(name = "freshPhone")
    private String freshPhone;

    @Column(name = "remarks")
    private String remarks;

}
