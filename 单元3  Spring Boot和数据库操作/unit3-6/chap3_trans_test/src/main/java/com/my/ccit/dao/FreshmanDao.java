package com.my.ccit.dao;

import com.my.ccit.entity.Freshman;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FreshmanDao extends JpaRepository<Freshman, Integer> {
}
