# 基于 MyBatis Plus 的图书馆书籍借阅管理案例

**一、项目准备**

1. 创建 Spring Boot 项目

   - 使用 IDEA创建一个新的 Spring Boot 项目，将包路径设置为 `com.demo.library` 。
   - 项目创建时选择依赖项：Lombok，Spring Boot DevTools，Spring Web，MySql Driver
2. 添加依赖
   - 在 `pom.xml` 文件中添加 MyBatis Plus 相关依赖：

```xml
<dependencies>
    <dependency>
        <groupId>com.baomidou</groupId>
        <artifactId>mybatis-plus-boot-starter</artifactId>
        <version>3.5.1</version>
    </dependency>
</dependencies>

```

```
# 配置文件内容
spring:
  datasource:
    url: jdbc:mysql://localhost:3306/library?useSSL=false&serverTimezone=UTC
    username: root
    password: root
    driver-class-name: com.mysql.cj.jdbc.Driver

mybatis-plus:
    mapper-locations: classpath:mapper/*.xml
    type-aliases-package: com.example.library.entity
    configuration:
      map-underscore-to-camel-case: true
      log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
```

**二、数据库设计**

1. 创建数据库
   创建名为 library 的数据库。
2. 创建表
   books 表（书籍表）

```sql
CREATE TABLE `books` (
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `title` VARCHAR(255),
    `author` VARCHAR(255),
    `publication_year` INT
);

INSERT INTO `books` (`title`, `author`, `publication_year`)
VALUES ('《Java 核心技术》', 'Cay S. Horstmann', 2019),
       ('《Python 编程从入门到实践》', 'Eric Matthes', 2016),
       ('《C++ Primer》', 'Stanley B. Lippman', 2020);
```

- borrow_records 表（借阅记录表）

```sql

CREATE TABLE `borrow_records` (
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `book_id` INT,
    `borrower_name` VARCHAR(255),
    `borrow_date` DATE,
    `return_date` DATE,
    FOREIGN KEY (`book_id`) REFERENCES `books`(`id`)
);

INSERT INTO `borrow_records` (`book_id`, `borrower_name`, `borrow_date`, `return_date`)
VALUES (1, '张三', '2023-07-01', '2023-07-15'),
       (2, '李四', '2023-07-05', '2023-07-20');
```

- book_categories 表（书籍分类表）

```sql
CREATE TABLE `book_categories` (
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `book_id` INT,
    `category_name` VARCHAR(255),
    FOREIGN KEY (`book_id`) REFERENCES `books`(`id`)
);

INSERT INTO `book_categories` (`book_id`, `category_name`)
VALUES (1, '编程'),
       (2, '编程语言'),
       (3, '编程');
```

**三、实体类创建**

- 创建 Book 实体类

```java

package com.demo.library.entity;

import lombok.Data;

@Data
@TableName("books")
public class Book {
     @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private String title;
    private String author;
    private Integer publicationYear;
}
```

- 创建 BorrowRecord 实体类

```java

package com.demo.library.entity;

import lombok.Data;

@Data
@TableName("borrow_records")
public class BorrowRecord {
     @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private Long bookId;
    private String borrowerName;
    private java.time.LocalDate borrowDate;
    private java.time.LocalDate returnDate;
}
```

- 创建 BookCategory 实体类

```java
package com.demo.library.entity;

import lombok.Data;

@Data
@TableName("book_categories")
public class BookCategory {
     @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private Long bookId;
    private String categoryName;
}
```

**四、Mapper 接口创建**

- 创建 BookMapper 接口

```java
package com.demo.library.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.library.entity.Book;

public interface BookMapper extends BaseMapper<Book> {}
```

- 创建 BorrowRecordMapper 接口

```java

package com.demo.library.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.library.entity.BorrowRecord;

public interface BorrowRecordMapper extends BaseMapper<BorrowRecord> {}
```

- 创建 BookCategoryMapper 接口

```java
package com.demo.library.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.library.entity.BookCategory;

public interface BookCategoryMapper extends BaseMapper<BookCategory> {}
```

**五、服务层创建**

- 创建 BookService 接口

```java
package com.demo.library.service;

import com.demo.library.entity.Book;
import com.demo.library.entity.BookCategory;

import java.util.List;

public interface BookService {
    List<Book> getAllBooks();
    Book getBookById(Long id);
    void addBook(Book book);
    void updateBook(Book book);
    void deleteBook(Long id);
    List<BookCategory> getCategoriesByBookId(Long bookId);
}
```

- 创建 BookServiceImpl 实现类

```java

package com.demo.library.service.impl;

import com.demo.library.entity.Book;
import com.demo.library.entity.BookCategory;
import com.demo.library.mapper.BookCategoryMapper;
import com.demo.library.mapper.BookMapper;
import com.demo.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookMapper bookMapper;

    @Autowired
    private BookCategoryMapper bookCategoryMapper;

    @Override
    public List<Book> getAllBooks() {
        return bookMapper.selectList(null);
    }

    @Override
    public Book getBookById(Long id) {
        return bookMapper.selectById(id);
    }

    @Override
    public void addBook(Book book) {
        bookMapper.insert(book);
    }

    @Override
    public void updateBook(Book book) {
        bookMapper.updateById(book);
    }

    @Override
    public void deleteBook(Long id) {
        bookMapper.deleteById(id);
    }

    @Override
    public List<BookCategory> getCategoriesByBookId(Long bookId) {
        return bookCategoryMapper.selectList(new LambdaQueryWrapper<BookCategory>().eq(BookCategory::getBookId, bookId));
    }
}
```

- 创建 BorrowRecordService 接口

```java

package com.demo.library.service;

import com.demo.library.entity.BorrowRecord;

import java.util.List;

public interface BorrowRecordService {
    List<BorrowRecord> getAllBorrowRecords();
    BorrowRecord getBorrowRecordById(Long id);
    void addBorrowRecord(BorrowRecord borrowRecord);
    void updateBorrowRecord(BorrowRecord borrowRecord);
    void deleteBorrowRecord(Long id);
}
```

- 创建 BorrowRecordServiceImpl 实现类

```java

package com.demo.library.service.impl;

import com.demo.library.entity.BorrowRecord;
import com.demo.library.mapper.BorrowRecordMapper;
import com.demo.library.service.BorrowRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BorrowRecordServiceImpl implements BorrowRecordService {

    @Autowired
    private BorrowRecordMapper borrowRecordMapper;

    @Override
    public List<BorrowRecord> getAllBorrowRecords() {
        return borrowRecordMapper.selectList(null);
    }

    @Override
    public BorrowRecord getBorrowRecordById(Long id) {
        return borrowRecordMapper.selectById(id);
    }

    @Override
    public void addBorrowRecord(BorrowRecord borrowRecord) {
        borrowRecordMapper.insert(borrowRecord);
    }

    @Override
    public void updateBorrowRecord(BorrowRecord borrowRecord) {
        borrowRecordMapper.updateById(borrowRecord);
    }

    @Override
    public void deleteBorrowRecord(Long id) {
        borrowRecordMapper.deleteById(id);
    }
}
```

```
书籍分类自行实现
```



**六、控制器层创建**

- 创建 BookController 控制器

```java
package com.demo.library.controller;

import com.demo.library.entity.Book;
import com.demo.library.entity.BookCategory;
import com.demo.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 图书管理的REST控制器
 */
@RestController
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;

    /**
     * 获取所有书籍
     *
     * @return 所有书籍的列表
     */
    @GetMapping("/getAll")
    public List<Book> getAllBooks() {
        return bookService.getAllBooks();
    }

    /**
     * 通过路径变量获取书籍
     *
     * @param id 书籍的ID
     * @return 指定ID的书籍
     */
    @PostMapping("/getBookById1/{id}")
    public Book getBookById1(@PathVariable Long id) {
        return bookService.getBookById(id);
    }

    /**
     * 通过请求参数获取书籍
     *
     * @param id 书籍的ID
     * @return 指定ID的书籍
     */
    @PostMapping("/getBookById2")
    public Book getBookById2(@RequestParam Long id) {
        return bookService.getBookById(id);
    }

    /**
     * 添加新书籍
     *
     * @param book 要添加的书籍
     * @return 成功消息
     */
    @PostMapping("/addBook")
    public String addBook(@RequestBody Book book) {
        bookService.addBook(book);
        return "Book added successfully";
    }

    /**
     * 更新现有书籍
     *
     * @param book 要更新的书籍
     */
    @PostMapping("/updateBook")
    public void updateBook(@RequestBody Book book) {
        bookService.updateBook(book);
    }

    /**
     * 通过ID删除书籍
     *
     * @param id 要删除的书籍ID
     */
    @PostMapping("/deleteBook/{id}")
    public void deleteBook(@PathVariable Long id) {
        bookService.deleteBook(id);
    }

    /**
     * 获取书籍的分类
     *
     * @param id 书籍的ID
     * @return 指定书籍的分类列表
     */
    @GetMapping("/categories/{id}")
    public List<BookCategory> getCategoriesByBookId(@PathVariable Long id) {
        return bookService.getCategoriesByBookId(id);
    }
}

```

- 创建 BorrowRecordController 控制器

```java
package com.demo.library.controller;

import com.demo.library.entity.BorrowRecord;
import com.demo.library.service.BorrowRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 借阅记录管理的REST控制器
 */
@RestController
@RequestMapping("/borrowRecord")
public class BorrowRecordController {

    @Autowired
    private BorrowRecordService borrowRecordService;

    /**
     * 获取所有借阅记录
     *
     * @return 所有借阅记录的列表
     */
    @GetMapping
    public List<BorrowRecord> getAllBorrowRecords() {
        return borrowRecordService.getAllBorrowRecords();
    }

    /**
     * 通过ID获取借阅记录
     *
     * @param id 借阅记录的ID
     * @return 指定ID的借阅记录
     */
    @GetMapping("/{id}")
    public BorrowRecord getBorrowRecordById(@PathVariable Long id) {
        return borrowRecordService.getBorrowRecordById(id);
    }

    /**
     * 添加新的借阅记录
     *
     * @param borrowRecord 要添加的借阅记录
     */
    @PostMapping
    public void addBorrowRecord(@RequestBody BorrowRecord borrowRecord) {
        borrowRecordService.addBorrowRecord(borrowRecord);
    }

    /**
     * 更新现有的借阅记录
     *
     * @param borrowRecord 要更新的借阅记录
     */
    @PutMapping
    public void updateBorrowRecord(@RequestBody BorrowRecord borrowRecord) {
        borrowRecordService.updateBorrowRecord(borrowRecord);
    }

    /**
     * 通过ID删除借阅记录
     *
     * @param id 要删除的借阅记录ID
     */
    @DeleteMapping("/{id}")
    public void deleteBorrowRecord(@PathVariable Long id) {
        borrowRecordService.deleteBorrowRecord(id);
    }
}

```

**七、运行测试**
启动应用，通过发送 HTTP 请求对各个接口进行测试。