package cn.edu.xzit.service;

import cn.edu.xzit.entity.User;

import java.util.List;

public interface UserService {
    public List<User> getAllUsers();

    public String getUserById(int id);
}
