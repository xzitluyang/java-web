package cn.edu.xzit.controller;

import cn.edu.xzit.entity.User;
import cn.edu.xzit.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/getAllUsers")
    public String getAllUsers() {
        List<User> userList = userService.getAllUsers();
        StringBuffer stf = new StringBuffer();
        for (User user : userList) {
            stf.append(user.toString());
            stf.append("<br>");
        }
        System.out.println(stf.toString());
        return stf.toString();
    }

    @GetMapping("/getUserById/{id}")
    public String getUserById(@PathVariable int id) {
        return userService.getUserById(id);
    }

}
