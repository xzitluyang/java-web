package cn.edu.xzit.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.edu.xzit.entity.User;
public interface UserMapper extends BaseMapper<User> {
}
