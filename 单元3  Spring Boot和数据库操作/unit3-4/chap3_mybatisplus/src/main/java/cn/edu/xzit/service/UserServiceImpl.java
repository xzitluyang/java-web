package cn.edu.xzit.service;

import cn.edu.xzit.mapper.UserMapper;
import cn.edu.xzit.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> getAllUsers() {
        System.out.println("----- selectAll method test ------");
        List<User> userList = userMapper.selectList(null);
        for(User user:userList){
            System.out.println(user);
        }
        return userList;
    }

    @Override
    public String getUserById(int id) {
        return userMapper.selectById(id).toString();
    }


}
