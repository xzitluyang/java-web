package cn.edu.xzit;

import cn.edu.xzit.mapper.UserMapper;
import cn.edu.xzit.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class TestMybatisPlusApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testSelect() {
        System.out.println(("----- selectAll method test ------"));
        List<User> userList = userMapper.selectList(null);
        for (User user : userList) {
            System.out.println(user);
        }
    }


    //  @Test
    public void testInsert() {
        System.out.println("(---- insert method test ----)");


        User user = new User();
        user.setName("rice");
        user.setAge(5);
        user.setEmail("rice@qq.com");
        int i = userMapper.insert(user);
        if (i > 0) {
            System.out.println("添加用户成功！");
        } else {
            System.out.println("添加用户失败！");
        }


    }

}
