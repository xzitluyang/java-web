package cn.edu.xzit.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.edu.xzit.entity.BusLine;



public interface BusLineMapper extends BaseMapper<BusLine> {
}
