package cn.edu.xzit.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.edu.xzit.entity.BusLine;
import cn.edu.xzit.mapper.BusLineMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BusLineServiceImpl implements BusLineService {
    @Autowired
    private BusLineMapper busLineMapper;


    @Override
    public boolean insertBusLine(BusLine busLine) {
        int i= busLineMapper.insert(busLine);
        return i>0?true:false;
    }

    @Override
    public boolean updateBusLine(BusLine busLine) {
        int i=busLineMapper.updateById(busLine);
        return i>0?true:false;
    }

    @Override
    public boolean deleteBusLineById(int id) {
        int i=busLineMapper.deleteById(id);
        return i>0?true:false;
    }

    @Override
    public List<BusLine> getAllBusLines() {
        return busLineMapper.selectList(null);
    }

    @Override
    public BusLine getBusLineById(int id) {
        return busLineMapper.selectById(id);
    }

    @Override
    public BusLine getBusLineByLineNo(String lineNo) {
        QueryWrapper<BusLine> wrapper=new QueryWrapper<>();
        wrapper.eq("lineNo",lineNo);
        return busLineMapper.selectOne(wrapper);
    }
}
