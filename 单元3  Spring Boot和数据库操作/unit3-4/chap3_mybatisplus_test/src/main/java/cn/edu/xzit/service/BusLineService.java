package cn.edu.xzit.service;

import cn.edu.xzit.entity.BusLine;

import java.util.List;

public interface BusLineService {


    /*
    添加公交线路、修改公交线路、删除公交线路、
    查询所有公交线路、根据名称查询公交线路、
    根据线路号查询公交线路信息。
     */

    public boolean insertBusLine(BusLine busLine);

    public boolean updateBusLine(BusLine busLine);

    public boolean deleteBusLineById(int id);

    public List<BusLine> getAllBusLines();

    public BusLine getBusLineById(int id);

    public BusLine getBusLineByLineNo(String lineNo);

}
