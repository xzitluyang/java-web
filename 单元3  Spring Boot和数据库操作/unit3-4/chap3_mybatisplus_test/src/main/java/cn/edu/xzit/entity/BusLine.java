package cn.edu.xzit.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value="tb_busline")
public class BusLine {
    private int id;

    @TableField(value="lineNo")
    private String lineNo;

    @TableField(value="startName")
    private String startName;

    @TableField(value="endName")
    private String endName;

    @TableField(value="startingTime")
    private String startTime;

    @TableField(value="endTime")
    private String endTime;

}
