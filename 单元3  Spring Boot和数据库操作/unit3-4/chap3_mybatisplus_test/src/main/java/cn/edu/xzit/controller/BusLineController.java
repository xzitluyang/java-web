package cn.edu.xzit.controller;

import cn.edu.xzit.service.BusLineService;
import cn.edu.xzit.entity.BusLine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BusLineController {

    @Autowired
    private BusLineService busLineService;

    @GetMapping("/getAllBusLines")
    public String getAllBusLines() {
        List<BusLine> busLineList = busLineService.getAllBusLines();
        StringBuffer stf = new StringBuffer();
        for (BusLine busLine :  busLineList) {
            stf.append(busLine.toString());
            stf.append("<br>");
        }
        System.out.println(stf.toString());
        return stf.toString();
    }

    @GetMapping("/getBusLineById/{id}")
    public String getUserById(@PathVariable int id) {
        return busLineService.getBusLineById(id).toString();
    }

    @GetMapping("/insert")
    public String  insertBusLine() {
        BusLine busLine=new BusLine();
        busLine.setLineNo("002");
        busLine.setStartName("青枫公园");
        busLine.setEndName("五一路");
        busLine.setStartTime("06:00");
        busLine.setEndTime("22:00");
        boolean result= busLineService.insertBusLine(busLine);
        if(result){
            return "添加路线成功！";
        }else{
            return "添加路线失败！";
        }
    }

    @GetMapping(value="/getBusLineByLineNo/{lineNo}")
    public BusLine getBusLineByLineNo(@PathVariable String lineNo) {
        return busLineService.getBusLineByLineNo(lineNo);
    }


    @GetMapping("/update")
    public String updateBusLine(BusLine busLine) {
        busLine.setId(2);
        busLine.setStartName("青枫公园");
        busLine.setEndName("开发区政府");
        busLine.setStartTime("06:30");
        busLine.setEndTime("22:50");

        boolean result= busLineService.updateBusLine(busLine);
        if(result){
            return "修改路线成功！";
        }else{
            return "修改路线失败！";
        }
    }

    @GetMapping("/deleteById/{id}")
    public String  deleteBusLineById(@PathVariable  int id) {
        boolean result= busLineService.deleteBusLineById(id);
        if(result){
            return "删除路线成功！";
        }else{
            return "删除路线失败！";
        }
    }


}
