package cn.edu.xzit.service;

import cn.edu.xzit.entity.Book;
import cn.edu.xzit.mapper.BookMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    private BookMapper bookMapper;


    @Override
    public boolean insertBook(Book book) {
        int i= bookMapper.insert(book);
        return i>0?true:false;
    }

    @Override
    public boolean updateBook(Book book) {
        int i=bookMapper.updateById(book);
        return i>0?true:false;
    }

    @Override
    public boolean deleteBookById(int id) {
        int i=bookMapper.deleteById(id);
        return i>0?true:false;
    }

    @Override
    public List<Book> getAllBooks() {
        return bookMapper.selectList(null);
    }

    @Override
    public Book getBookById(int id) {
        return bookMapper.selectById(id);
    }

    @Override
    public Book getBookByIsbn(String isbn) {
        QueryWrapper<Book> wrapper=new QueryWrapper<>();
        wrapper.eq("isbn",isbn);
        return bookMapper.selectOne(wrapper);
    }
}
