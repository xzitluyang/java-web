package cn.edu.xzit;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("cn.edu.xzit.mapper")
@SpringBootApplication
public class Chap3MybatisBookPlusApplication {

    public static void main(String[] args) {
        SpringApplication.run(Chap3MybatisBookPlusApplication.class, args);
    }

}
