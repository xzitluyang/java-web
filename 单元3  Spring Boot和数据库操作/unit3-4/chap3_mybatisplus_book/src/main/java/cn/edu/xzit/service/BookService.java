package cn.edu.xzit.service;

import cn.edu.xzit.entity.Book;

import java.util.List;

public interface BookService {


    /*
    图书增加、图书删除、图书修改、图书查询、图书显示。
     */

    public boolean insertBook(Book book);

    public boolean updateBook(Book book);

    public boolean deleteBookById(int id);

    public List<Book> getAllBooks();

    public Book getBookById(int id);

    public Book getBookByIsbn(String isbn);

}
