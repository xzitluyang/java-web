package cn.edu.xzit.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value="tb_book")
public class Book {
    private int id;


    private String isbn;

    @TableField(value="bookName")
    private String bookName;
    private String writer;
    private String press;
    private double price;
    private String  remarks;
}
