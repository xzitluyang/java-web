package cn.edu.xzit.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.edu.xzit.entity.Book;


public interface BookMapper extends BaseMapper<Book> {
}
