package cn.edu.xzit.dao;

import cn.edu.xzit.entity.SysAssetType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface SysAssetTypeDao extends JpaRepository<SysAssetType,Integer> {

    List<SysAssetType> getSysAssetTypeByIdEquals(Integer id);

    List<SysAssetType> getSysAssetTypeByNameStartingWith(String name);

    @Query("select s from sys_asset_type s")
    List<SysAssetType> getAllSysAssetType();

    List<SysAssetType> getAllSysAssetTypeBySuperId(Integer super_id);

}
