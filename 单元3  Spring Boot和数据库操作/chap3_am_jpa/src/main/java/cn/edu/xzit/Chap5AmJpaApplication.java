package cn.edu.xzit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Chap5AmJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(Chap5AmJpaApplication.class, args);
    }

}
