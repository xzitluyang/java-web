package cn.edu.xzit.service;

import cn.edu.xzit.entity.SysAssetType;

import java.util.List;

public interface SysAssetTypeService {

    public void saveSysAssetType(SysAssetType sysAssetType);

    public List<SysAssetType> getSysAssetTypeByIdEquals(Integer id);

    public List<SysAssetType> getSysAssetTypeByNameStartingWith(String name);

    public List<SysAssetType> getAllSysAssetType();

    public List<SysAssetType> getAllSysAssetTypeBySupserId(Integer super_id);



}
