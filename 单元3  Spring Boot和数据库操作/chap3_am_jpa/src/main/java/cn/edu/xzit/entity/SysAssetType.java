package cn.edu.xzit.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name="sys_asset_type")
@Data
public class SysAssetType implements Serializable {

    private static final long serialVersionUID = 7327693442597506148L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long pid;

    private String name;

    @Column(name="create_time",nullable = false)
    private Long createTime;

    private Long createBy;

    private Long updateTime;

    private Long updateBy;

    @Column(name="super_id",nullable = false)
    private Long superId;

    private Integer delFlag;

    public SysAssetType(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Long getSuperId() {
        return superId;
    }

    public void setSuperId(Long superId) {
        this.superId = superId;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "SysAssetType{" +
                "id=" + id +
                ", pid=" + pid +
                ", name='" + name + '\'' +
                ", createTime=" + createTime +
                ", createBy=" + createBy +
                ", updateTime=" + updateTime +
                ", updateBy=" + updateBy +
                ", superId=" + superId +
                ", delFlag=" + delFlag +
                '}';
    }
}
