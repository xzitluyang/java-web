package cn.edu.xzit.controller;


import cn.edu.xzit.service.SysAssetTypeService;
import cn.edu.xzit.entity.SysAssetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class SysAssetTypeController {

    @Autowired
    SysAssetTypeService sysAssetTypeService;


    @GetMapping("/save")
    public void saveSysAssetType() {

        SysAssetType sysAssetType=new SysAssetType();
        sysAssetType.setPid(2l);
        sysAssetType.setName("打印机");

        long createTime=(new Date()).getTime();
        System.out.println("createTime="+createTime);
        sysAssetType.setCreateTime(createTime);
        sysAssetType.setCreateBy(null);
        sysAssetType.setUpdateTime(null);
        sysAssetType.setUpdateBy(null);
        sysAssetType.setSuperId(2l);
        sysAssetType.setDelFlag(0);

        System.out.println(sysAssetType.getSuperId());
        sysAssetTypeService.saveSysAssetType(sysAssetType);

        System.out.println(sysAssetType);

    }

    @GetMapping("/getAll")
    public void getAllSysAssetType() {

         List<SysAssetType> sysAssetTypeList= sysAssetTypeService.getAllSysAssetType();
         System.out.println(sysAssetTypeList.size());

         System.out.println(sysAssetTypeList);

    }

    @GetMapping("/getAllBySuperId/")
    public void getAllSysAssetType(@PathVariable  Integer superId) {

        List<SysAssetType> sysAssetTypeList= sysAssetTypeService.getAllSysAssetType();
        System.out.println(sysAssetTypeList.size());

        System.out.println(sysAssetTypeList);

    }



}
