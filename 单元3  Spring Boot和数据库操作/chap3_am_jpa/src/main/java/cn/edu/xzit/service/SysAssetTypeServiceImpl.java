package cn.edu.xzit.service;

import cn.edu.xzit.dao.SysAssetTypeDao;
import cn.edu.xzit.entity.SysAssetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysAssetTypeServiceImpl  implements  SysAssetTypeService{

    @Autowired
    SysAssetTypeDao sysAssetTypeDao;

    @Override
    public void saveSysAssetType(SysAssetType sysAssetType) {
        sysAssetTypeDao.save(sysAssetType);
    }

    @Override
    public List<SysAssetType> getSysAssetTypeByIdEquals(Integer id) {
        return  sysAssetTypeDao.getSysAssetTypeByIdEquals(id);
    }

    @Override
    public List<SysAssetType> getSysAssetTypeByNameStartingWith(String name) {
        return sysAssetTypeDao.getSysAssetTypeByNameStartingWith(name);
    }

    @Override
    public List<SysAssetType> getAllSysAssetType() {
        return sysAssetTypeDao.getAllSysAssetType();
    }

    @Override
    public List<SysAssetType> getAllSysAssetTypeBySupserId(Integer super_id) {
        return null;
    }
}
